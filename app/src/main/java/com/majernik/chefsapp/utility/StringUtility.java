package com.majernik.chefsapp.utility;

import java.text.DecimalFormat;

public class StringUtility
{
    final public static String FRACTION_HALF = "½";
    final public static String FRACTION_THIRD = "⅓";
    final public static String FRACTION_QUARTER = "¼";
    final public static String FRACTION_EIGHT = "⅛";

    final public static int ROTATION_DIRECTION_LEFT = 1;
    final public static int ROTATION_DIRECTION_RIGHT = 2;

    public static String rotateByThird(String text, int direction)
    {
        int shiftBy = (int) Math.ceil((double) text.length() / 3);

        return rotate(text, shiftBy, direction);
    }

    public static String getFormattedDoubleWithFractions(Double number)
    {
        if (number == 0.5) {
            return FRACTION_HALF;
        } else if (number == 0.33) {
            return FRACTION_THIRD;
        } else if (number == 0.25) {
            return FRACTION_QUARTER;
        } else if (number == 0.125) {
            return FRACTION_EIGHT;
        }
        return getFormattedDouble(number);
    }

    public static String getFormattedDouble(Double number)
    {
        DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
        format.setMaximumFractionDigits(1);
        format.setDecimalSeparatorAlwaysShown(false);

        return format.format(number);
    }

    public static String rotate(String text, int shiftBy, int direction)
    {
        for (int i = 0; i < shiftBy; i++) {
            if (direction == ROTATION_DIRECTION_LEFT) {
                text = text.substring(1, text.length()) + text.substring(0, 1);
            } else {
                text = text.substring(text.length() - 1) + text.substring(0, text.length() - 1);
            }
        }
        return text;
    }

    public static int computeLevenshteinDistance(CharSequence lhs, CharSequence rhs)
    {
        int[][] distance = new int[lhs.length() + 1][rhs.length() + 1];

        for (int i = 0; i <= lhs.length(); i++) {
            distance[i][0] = i;
        }
        for (int j = 1; j <= rhs.length(); j++) {
            distance[0][j] = j;
        }

        for (int i = 1; i <= lhs.length(); i++) {
            for (int j = 1; j <= rhs.length(); j++) {
                distance[i][j] = minimum(
                        distance[i - 1][j] + 1,
                        distance[i][j - 1] + 1,
                        distance[i - 1][j - 1] + ((lhs.charAt(i - 1) == rhs.charAt(
                                j - 1)) ? 0 : 1));
            }
        }

        return distance[lhs.length()][rhs.length()];
    }

    private static int minimum(int a, int b, int c)
    {
        return Math.min(Math.min(a, b), c);
    }
}
