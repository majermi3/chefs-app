package com.majernik.chefsapp.utility;

public final class Session
{
    private static String token;

    public static void setToken(String t)
    {
        token = t;
    }

    public static void clearToken()
    {
        token = null;
    }

    public static String getToken()
    {
        return token;
    }

    public static boolean hasToken()
    {
        return token != null && !token.isEmpty();
    }
}
