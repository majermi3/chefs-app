package com.majernik.chefsapp.utility;

import java.util.Arrays;

public class FileExtensionUtility
{
    public static final String[] IMG_FORMATS = {"jpg", "jpeg", "gif", "png", "bmp"};

    public static final String[] VIDEO_FORMATS = {"mp4"};

    public static boolean isImage(String fileName)
    {
        return Arrays.asList(IMG_FORMATS).contains(getExtension(fileName));
    }

    public static boolean isVideo(String fileName)
    {
        return Arrays.asList(VIDEO_FORMATS).contains(getExtension(fileName));
    }

    public static String getExtension(String fileName)
    {
        int dot = fileName.lastIndexOf(".");
        return fileName.substring(dot + 1);
    }
}
