package com.majernik.chefsapp.utility;

import android.content.res.Resources;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.model.Settings;

public class UnitsUtility
{
    public static String getUnitName(Resources resources, Settings settings, int position,
                                     boolean isLiquid)
    {
        String[] unitNames = getUnitNames(resources, settings, isLiquid);
        if (position < unitNames.length) {
            return unitNames[position];
        }
        return "";
    }

    public static String[] getUnitNames(Resources resources, Settings settings, boolean isLiquid)
    {
        if (isLiquid) {
            if (settings.getUnitsSystem() == Settings.UNITS_METRIC) {
                return resources.getStringArray(R.array.units_volume);
            } else {
                return resources.getStringArray(R.array.units_volume_imperial);
            }
        } else {
            if (settings.getUnitsSystem() == Settings.UNITS_METRIC) {
                return resources.getStringArray(R.array.units_weight);
            } else {
                return resources.getStringArray(R.array.units_weight_imperial);
            }
        }
    }

    public static Double getConvertedAmount(Settings settings, RecipeIngredient ingredient)
    {
        if ((settings.isMetricSystem() && ingredient.isMetricUnitSystem())
                || (!settings.isMetricSystem() && !ingredient.isMetricUnitSystem())) {
            return ingredient.getAmount();
        }
        if (settings.isMetricSystem()) {
            // convert to the metric system
            return convertToMetricUnits(ingredient);
        } else {
            // convert to the imperial system
            return convertToImperialUnits(ingredient);
        }
    }

    private static Double convertToMetricUnits(RecipeIngredient ingredient)
    {
        if (ingredient.getIngredient().isLiquid()) {
            return convertVolumeToMetricUnits(ingredient);
        } else {
            return convertWeightToMetricUnits(ingredient);
        }
    }

    private static Double convertToImperialUnits(RecipeIngredient ingredient)
    {
        if (ingredient.getIngredient().isLiquid()) {
            return convertVolumeToImperialUnits(ingredient);
        } else {
            return convertWeightToImperialUnits(ingredient);
        }
    }

    private static Double convertVolumeToMetricUnits(RecipeIngredient ingredient)
    {
        switch (ingredient.getUnit()) {
            case 0: // convert "fl oz" to ml
                return ingredient.getAmount() * 28.413;
            case 1: // convert qt to l
                return ingredient.getAmount() * 1.136;
            default:
                return ingredient.getAmount();
        }
    }

    private static Double convertVolumeToImperialUnits(RecipeIngredient ingredient)
    {
        switch (ingredient.getUnit()) {
            case 0: // convert "fl oz" to ml
                return ingredient.getAmount() / 28.413;
            case 1: // convert qt to l
                return ingredient.getAmount() / 1.136;
            default:
                return ingredient.getAmount();
        }
    }

    private static Double convertWeightToMetricUnits(RecipeIngredient ingredient)
    {
        switch (ingredient.getUnit()) {
            case 0: // convert dr to g
                return ingredient.getAmount() * 1.771;
            case 1: // convert oz to dkg
                return ingredient.getAmount() * 2.8349;
            case 2: // convert lb to kg
                return ingredient.getAmount() * 0.4535;
            default:
                return ingredient.getAmount();
        }
    }

    private static Double convertWeightToImperialUnits(RecipeIngredient ingredient)
    {
        switch (ingredient.getUnit()) {
            case 0: // convert g to dr
                return ingredient.getAmount() / 1.771;
            case 1: // convert dkg to oz
                return ingredient.getAmount() / 2.8349;
            case 2: // convert kg to lb
                return ingredient.getAmount() / 0.4535;
            default:
                return ingredient.getAmount();
        }
    }
}
