package com.majernik.chefsapp.presenter;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.contract.DashboardContract;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.service.UserService;

public class DashboardPresenter implements DashboardContract.Presenter
{

    private DashboardContract.View dashboardView;
    private UserService userService;

    public DashboardPresenter(DashboardContract.View dashboardView)
    {
        this.dashboardView = dashboardView;
        userService = new UserService(dashboardView.getApplicationContext());
    }

    @Override
    public void markRecipeAsFavorite(Recipe recipe,
                                     ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        userService.markRecipeAsFavorite(recipe, onFinishedListener);
    }

    @Override
    public void unmarkRecipeAsFavorite(Recipe recipe,
                                       ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        userService.unmarkRecipeAsFavorite(recipe, onFinishedListener);
    }
}
