package com.majernik.chefsapp.presenter;

import com.majernik.chefsapp.contract.MainContract;

public class MainPresenter implements MainContract.Presenter
{
    private MainContract.View view;

    public MainPresenter(MainContract.View view)
    {
        this.view = view;
    }

    @Override
    public void login()
    {

    }

    @Override
    public void register()
    {

    }
}
