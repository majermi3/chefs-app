package com.majernik.chefsapp.presenter;

import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.contract.SignUpContract;
import com.majernik.chefsapp.model.User;
import com.majernik.chefsapp.service.UserService;

public class SignUpPresenter implements SignUpContract.Presenter, ApiServiceInterface.OnFinishedListener
{
    private SignUpContract.View suView;
    private UserService userService;

    public SignUpPresenter(SignUpContract.View suView)
    {
        this.suView = suView;
        userService = new UserService(suView.getApplicationContext());
    }

    public void signUp(User user)
    {
        userService.createUser(user, this);
    }

    @Override
    public void onFinished(ApiResponse response)
    {
        if (response.isSuccessful()) {
            suView.showLogin();
        } else {
            suView.showError(response.getMessage());
        }
    }

    @Override
    public void onFailure(Throwable t)
    {

    }
}
