package com.majernik.chefsapp.presenter;

import android.content.Context;
import android.graphics.Bitmap;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.contract.CreateRecipeContract;
import com.majernik.chefsapp.model.Ingredient;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.activity.adapter.listener.OnIngredientClickListener;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.model.RecipeStep;
import com.majernik.chefsapp.service.IngredientsService;
import com.majernik.chefsapp.service.RecipeIngredientsService;
import com.majernik.chefsapp.service.RecipeService;
import com.majernik.chefsapp.service.RecipeStepsService;
import com.majernik.chefsapp.service.SettingsService;

import java.io.IOException;
import java.util.TreeMap;

public class CreateRecipePresenter implements CreateRecipeContract.Presenter, ApiServiceInterface.IngredientService.OnSearchFinishedListener, OnIngredientClickListener
{
    private CreateRecipeContract.View crView;
    private IngredientsService ingredientsService;
    private RecipeIngredientsService recipeIngredientsService;
    private RecipeStepsService recipeStepsService;
    private RecipeService recipeService;
    private SettingsService settingsService;

    private Recipe recipe;

    public CreateRecipePresenter(CreateRecipeContract.View crView, Recipe recipe)
    {
        Context context = crView.getApplicationContext();

        this.crView = crView;
        this.recipe = recipe;
        this.recipeService = new RecipeService(context);
        this.ingredientsService = new IngredientsService(context);
        this.recipeStepsService = new RecipeStepsService(context);
        this.recipeIngredientsService = new RecipeIngredientsService(context);
        this.settingsService = new SettingsService(context);

        if (this.recipe.getId() == null) {
            persistRecipe();
        }
    }

    @Override
    public void loadIngredients()
    {

    }

    @Override
    public void createIngredient(final RecipeIngredient recipeIngredient)
    {
        if (recipeIngredient.getIngredient().getId() != null) {
            addRecipeIngredient(recipeIngredient);
        } else {
            ingredientsService.createIngredient(recipeIngredient.getIngredient(),
                    new ApiServiceInterface.OnFinishedListener()
                    {
                        @Override
                        public void onFinished(ApiResponse response)
                        {
                            //TODO update ingredient in the list view so it won't be created more then once
                            recipeIngredient.getIngredient().setId(response.getId());
                            addRecipeIngredient(recipeIngredient);
                        }

                        @Override
                        public void onFailure(Throwable t)
                        {
                            System.out.println(t.getMessage());
                        }
                    });
        }
    }

    public void addRecipeIngredient(final RecipeIngredient recipeIngredient)
    {
        recipeIngredientsService.createRecipeIngredient(recipe, recipeIngredient,
                new ApiServiceInterface.OnFinishedListener()
                {
                    @Override
                    public void onFinished(ApiResponse response)
                    {
                        if (response.isSuccessful()) {
                            recipeIngredient.setId(response.getId());
                            recipe.addIngredient(recipeIngredient);
                            crView.addIngredientToView(recipeIngredient);
                        }
                    }

                    @Override
                    public void onFailure(Throwable t)
                    {
                        System.out.println(t.getMessage());
                    }
                });
    }

    /**
     * Removes ingredient from recipe
     *
     * @param recipeIngredient
     */
    @Override
    public void removeIngredient(final RecipeIngredient recipeIngredient)
    {
        recipeIngredientsService.removeRecipeIngredient(recipe, recipeIngredient,
                new ApiServiceInterface.OnFinishedListener()
                {
                    @Override
                    public void onFinished(ApiResponse response)
                    {
                        recipe.removeIngredient(recipeIngredient);
                        crView.removeIngredientFromView(recipeIngredient);
                    }

                    @Override
                    public void onFailure(Throwable t)
                    {
                        System.out.println(t.getMessage());
                    }
                });
    }

    @Override
    public void findIngredients(CharSequence s)
    {
        ingredientsService.findIngredient(s, this);
    }

    @Override
    public void persistRecipe()
    {
        recipeService.saveRecipe(recipe, new ApiServiceInterface.OnFinishedListener()
        {
            @Override
            public void onFinished(ApiResponse response)
            {
                if (response.isSuccessful()) {
                    recipe.setId(response.getId());
                }
            }

            @Override
            public void onFailure(Throwable t)
            {
                System.out.println(t.getMessage());
            }
        });
    }

    @Override
    public void setRecipeTitle(String title)
    {
        recipe.setTitle(title);
    }

    @Override
    public void setRecipeDescription(String description)
    {
        recipe.setDescription(description);
    }

    @Override
    public void setRecipeAccessLevel(int accessLevel)
    {
        recipe.setAccessLevel(accessLevel);
    }

    public void updateRecipeStep(final RecipeStep step)
    {
        recipeStepsService.saveRecipeStep(recipe, step, new ApiServiceInterface.OnFinishedListener()
        {
            @Override
            public void onFinished(ApiResponse response)
            {
                if (response.isSuccessful()) {
                    recipe.setStep(step);
                    crView.notifyStepChanged(step);
                }
            }

            @Override
            public void onFailure(Throwable t)
            {

            }
        });
    }

    @Override
    public void addRecipeStep(final RecipeStep step)
    {
        recipeStepsService.saveRecipeStep(recipe, step, new ApiServiceInterface.OnFinishedListener()
        {
            @Override
            public void onFinished(ApiResponse response)
            {
                if (response.isSuccessful()) {
                    step.setId(response.getId());
                    recipe.addStep(step);
                    crView.notifyStepAdded();
                }
            }

            @Override
            public void onFailure(Throwable t)
            {
                System.out.println("API Error: " + t.getMessage());
            }
        });
    }

    @Override
    public void uploadRecipeImage(Bitmap image)
    {
        try {
            recipeService.uploadRecipeImage(recipe, image,
                    new ApiServiceInterface.OnMediaUploadedListener()
                    {
                        @Override
                        public void onFinished(MediaResponse response)
                        {
                            if (response.isSuccessful()) {
                                recipe.setImage(response.getPath());
                            }
                        }

                        @Override
                        public void onFailure(Throwable t)
                        {

                        }
                    });
        } catch (IOException e) {


        }
    }

    private void shiftRecipeSteps(int offset)
    {
        for (RecipeStep step : recipe.getSteps()) {
            if (step.getPosition() > offset) {
                step.setPosition(step.getPosition() - 1);
                updateRecipeStep(step);
            }
        }
    }

    public void removeRecipeStep(final RecipeStep step)
    {
        recipeStepsService.removeRecipeStep(recipe, step,
                new ApiServiceInterface.OnFinishedListener()
                {
                    @Override
                    public void onFinished(ApiResponse response)
                    {
                        if (response.isSuccessful()) {
                            int position = step.getPosition();
                            recipe.removeStep(step);
                            shiftRecipeSteps(position);
                            crView.notifyStepsChanged(recipe.getSteps());
                        }
                    }

                    @Override
                    public void onFailure(Throwable t)
                    {

                    }
                });
    }

    @Override
    public void onFinished(TreeMap<Integer, Ingredient> distanceMap)
    {
        crView.getIngredientsPopup().showIngredients(distanceMap);
    }

    @Override
    public void onFailure(Throwable t)
    {

    }

    @Override
    public void onIngredientClick(Ingredient ingredient)
    {
        RecipeIngredient recipeIngredient = new RecipeIngredient(ingredient);
        recipe.addIngredient(recipeIngredient);
        crView.addIngredientToView(recipeIngredient);
    }

    public Recipe getRecipe()
    {
        return recipe;
    }
}
