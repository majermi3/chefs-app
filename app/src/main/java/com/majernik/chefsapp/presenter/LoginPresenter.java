package com.majernik.chefsapp.presenter;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.TokenResponse;
import com.majernik.chefsapp.contract.LoginContract;
import com.majernik.chefsapp.service.UserService;

public class LoginPresenter implements LoginContract.Presenter, ApiServiceInterface.TokenService.OnFinishedListener
{
    private LoginContract.View lView;
    private UserService userService;

    public LoginPresenter(LoginContract.View lView)
    {
        this.lView = lView;
        this.userService = new UserService(lView.getApplicationContext());
    }

    @Override
    public void loginUser(String email, String password)
    {
        userService.loginUser(email, password, this);
    }

    @Override
    public void onFinished(TokenResponse tokenResponse)
    {
        if (tokenResponse.isSuccessful()) {
            lView.showDashboard();
        } else {
            lView.showError(tokenResponse.getMessage());
        }
    }

    @Override
    public void onFailure(Throwable t)
    {
        lView.showError(t.getMessage());
    }
}
