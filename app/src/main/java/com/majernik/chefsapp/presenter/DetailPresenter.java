package com.majernik.chefsapp.presenter;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.contract.DetailContract;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.service.RecipeService;

public class DetailPresenter implements DetailContract.Presenter
{
    private RecipeService recipeService;
    private DetailContract.View detailActivity;

    public DetailPresenter(DetailContract.View detailActivity)
    {
        this.detailActivity = detailActivity;
        recipeService = new RecipeService(detailActivity.getApplicationContext());
    }

    @Override
    public void deleteRecipe(Recipe recipe)
    {
        recipeService.deleteRecipe(recipe, new ApiServiceInterface.OnFinishedListener()
        {
            @Override
            public void onFinished(ApiResponse response)
            {
                if (response.isSuccessful()) {
                    detailActivity.showMyRecipesView();
                }
            }

            @Override
            public void onFailure(Throwable t)
            {
                System.out.println(t.getMessage());
            }
        });
    }
}
