package com.majernik.chefsapp.presenter;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.contract.CreateRecipeStepContract;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeStep;
import com.majernik.chefsapp.service.RecipeStepsService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreateRecipeStepPresenter implements CreateRecipeStepContract.Presenter
{

    private RecipeStep step;
    private Recipe recipe;
    private CreateRecipeStepContract.View mView;
    private RecipeStepsService recipeStepsService;

    public CreateRecipeStepPresenter(CreateRecipeStepContract.View view, Recipe recipe,
                                     RecipeStep step)
    {
        Context context = view.getApplicationContext();
        mView = view;
        this.recipe = recipe;
        this.step = step;
        recipeStepsService = new RecipeStepsService(context);
    }

    public void uploadThumbnail(Bitmap thumbnail)
    {

    }

    @Override
    public void uploadRecipeStepImage(Bitmap imageBitmap)
    {
        try {
            recipeStepsService.uploadRecipeStepImage(recipe, step, imageBitmap,
                    new ApiServiceInterface.OnMediaUploadedListener()
                    {
                        @Override
                        public void onFinished(MediaResponse response)
                        {
                            step.setMedia(response.getPath());
                        }

                        @Override
                        public void onFailure(Throwable t)
                        {
                            System.out.println(t.getMessage());
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void uploadRecipesStepThumbnail(Bitmap thumbnail)
    {
        try {
            recipeStepsService.uploadRecipeStepThumbnail(recipe, step, thumbnail,
                    new ApiServiceInterface.OnMediaUploadedListener()
                    {
                        @Override
                        public void onFinished(MediaResponse response)
                        {
                            step.setThumbnail(response.getPath());
                        }

                        @Override
                        public void onFailure(Throwable t)
                        {
                            System.out.println(t.getMessage());
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Bitmap uploadRecipeStepVideo(AssetFileDescriptor videoAsset)
    {
        File video = writeVideoCaptureToFile(videoAsset);
        Bitmap thumbnail = null;

        if (video != null) {
            thumbnail = ThumbnailUtils.createVideoThumbnail(video.getAbsolutePath(),
                    MediaStore.Video.Thumbnails.MICRO_KIND);

            uploadRecipesStepThumbnail(thumbnail);

            try {
                recipeStepsService.uploadRecipeStepVideo(recipe, step, video,
                        new ApiServiceInterface.OnMediaUploadedListener()
                        {
                            @Override
                            public void onFinished(MediaResponse response)
                            {
                                step.setMedia(response.getPath());
                            }

                            @Override
                            public void onFailure(Throwable t)
                            {
                                System.out.println(t.getMessage());
                            }
                        });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return thumbnail;
    }

    private File writeVideoCaptureToFile(AssetFileDescriptor videoAsset)
    {
        try {
            FileInputStream fis = videoAsset.createInputStream();

            File tmpFile = new File(Environment.getExternalStorageDirectory(), "VideoCapture.mp4");
            FileOutputStream fos = new FileOutputStream(tmpFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = fis.read(buf)) > 0) {
                fos.write(buf, 0, len);
            }
            fis.close();
            fos.close();

            return tmpFile;
        } catch (IOException io_e) {
            System.out.println(io_e.getMessage());
        }

        return null;
    }
}
