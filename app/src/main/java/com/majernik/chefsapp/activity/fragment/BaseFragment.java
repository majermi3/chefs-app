package com.majernik.chefsapp.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.UserResponse;
import com.majernik.chefsapp.model.User;
import com.majernik.chefsapp.service.UserService;

abstract public class BaseFragment extends Fragment
{

    protected User user;

    protected UserService userService;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        userService.getCurrentUser(new ApiServiceInterface.UserService.OnFinishedListener()
        {
            @Override
            public void onFinished(UserResponse userResponse)
            {
                user = userResponse.getUser();
                userLoaded();
            }

            @Override
            public void onFailure(Throwable t)
            {

            }
        });
        super.onCreate(savedInstanceState);
    }

    abstract protected void userLoaded();

    @Override
    public void onAttach(Context context)
    {
        userService = new UserService(context);
        super.onAttach(context);
    }

    protected void showToast(String message)
    {
        Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }
}
