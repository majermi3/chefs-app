package com.majernik.chefsapp.activity.fragment.friends;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.UserListAdapter;
import com.majernik.chefsapp.activity.adapter.listener.PendingFriendshipListener;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.model.User;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

public class PendingRequestsFragment extends BaseFriendsFragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_pending_requests, container, false);

        userList = view.findViewById(R.id.users_list);

        if (user != null && userListAdapter == null) {
            initUserList(user.getPendingRequests());
        }

        return view;
    }

    @Override
    protected void userLoaded()
    {
        if (userListAdapter == null && view != null) {
            initUserList(user.getPendingRequests());
        }
    }

    private void initUserList(List<User> users)
    {
        userListAdapter = new UserListAdapter(users, new PendingFriendshipListener()
        {
            @Override
            public void acceptPendingFriendship(final User friend)
            {
                userService.acceptFriendshipRequest(friend.getId(),
                        new ApiServiceInterface.OnFinishedListener()
                        {
                            @Override
                            public void onFinished(ApiResponse response)
                            {
                                showToast("Friendship request accepted");
                                user.addFriend(friend);
                                userListAdapter.removeUser(friend);
                            }

                            @Override
                            public void onFailure(Throwable t)
                            {
                                showToast("Friendship request could not be accepted");
                                System.out.println(t.getMessage());
                            }
                        });
            }

            @Override
            public void rejectPendingFriendship(final User friend)
            {
                userService.rejectFriendshipRequest(friend.getId(),
                        new ApiServiceInterface.OnFinishedListener()
                        {
                            @Override
                            public void onFinished(ApiResponse response)
                            {
                                showToast("Friendship request rejected");
                                user.removePendingRequest(friend);
                                userListAdapter.removeUser(friend);
                            }

                            @Override
                            public void onFailure(Throwable t)
                            {
                                showToast("Friendship request could not be rejected");
                                System.out.println(t.getMessage());
                            }
                        });
            }
        });
        userList.setAdapter(userListAdapter);
        userList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * @return A new instance of fragment PendingRequestsFragment.
     */
    public static PendingRequestsFragment newInstance()
    {
        return new PendingRequestsFragment();
    }
}
