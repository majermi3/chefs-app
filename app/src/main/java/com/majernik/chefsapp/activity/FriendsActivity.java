package com.majernik.chefsapp.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.UserListAdapter;
import com.majernik.chefsapp.activity.adapter.listener.NewFriendshipListener;
import com.majernik.chefsapp.activity.adapter.pager.FriendsPagerAdapter;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.UsersResponse;
import com.majernik.chefsapp.model.User;

import java.util.List;

public class FriendsActivity extends BaseSearchableActivity
{
    private ViewPager slideViewPager;
    private TabLayout tabs;

    protected RecyclerView userList;
    protected UserListAdapter userListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            setContentView(R.layout.activity_friend_search);
            super.onCreate(savedInstanceState);

            userList = findViewById(R.id.users_list);

            String query = intent.getStringExtra(SearchManager.QUERY);

            userService.findUsers(query,
                    new ApiServiceInterface.UserService.OnUsersFinishedListener()
                    {
                        @Override
                        public void onFinished(UsersResponse usersResponse)
                        {
                            initUserList(usersResponse.getUsers());
                        }

                        @Override
                        public void onFailure(Throwable t)
                        {
                            showToast(R.string.toast__friends__search_not_available);
                        }
                    });
        } else {
            setContentView(R.layout.activity_friends);
            super.onCreate(savedInstanceState);
        }
    }

    @Override
    protected void userLoaded()
    {
        Intent intent = getIntent();

        if (!Intent.ACTION_SEARCH.equals(intent.getAction())) {
            slideViewPager = findViewById(R.id.slideViewPager);

            FriendsPagerAdapter pageAdapter = new FriendsPagerAdapter(
                    getSupportFragmentManager(),
                    2
            );
            slideViewPager.setAdapter(pageAdapter);
            setUpTabs();

            if (intent.hasExtra("redirect")) {
                slideViewPager.setCurrentItem(
                        pageAdapter.getFragmentPosition(intent.getStringExtra("redirect"))
                );
            }
        }
    }

    protected void initUserList(List<User> users)
    {
        userListAdapter = new UserListAdapter(users, new NewFriendshipListener()
        {

            @Override
            public void onFriendshipRequest(final User friend)
            {
                userService.sendFriendshipRequest(friend.getId(),
                        new ApiServiceInterface.OnFinishedListener()
                        {
                            @Override
                            public void onFinished(ApiResponse response)
                            {
                                showToast(R.string.toast__friends__request_send);
                                redirectAndFinish(FriendsActivity.class);
                            }

                            @Override
                            public void onFailure(Throwable t)
                            {
                                showToast(R.string.toast__friends__request_not_send);
                                System.out.println(t.getMessage());
                            }
                        });
            }
        });
        userList.setAdapter(userListAdapter);
        userList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    private void setUpTabs()
    {
        tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(slideViewPager);
        tabs.getTabAt(0).setIcon(R.drawable.ic_people_white_24dp);
        tabs.getTabAt(1).setIcon(R.drawable.ic_watch_later_white_24dp);
    }
}
