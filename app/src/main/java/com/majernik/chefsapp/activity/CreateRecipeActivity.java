package com.majernik.chefsapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.AccessLevelAdapter;
import com.majernik.chefsapp.activity.adapter.callback.ItemTouchHelperCallback;
import com.majernik.chefsapp.activity.adapter.listener.OnIngredientDeleteClickListener;
import com.majernik.chefsapp.activity.adapter.RecipeStepsListAdapter;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeStepDeleteClickListener;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeStepEditClickListener;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeStepThumbnailClickListener;
import com.majernik.chefsapp.activity.popup.IngredientsPopup;
import com.majernik.chefsapp.api.UrlUtility;
import com.majernik.chefsapp.contract.CreateRecipeContract;
import com.majernik.chefsapp.activity.adapter.IngredientListAdapter;
import com.majernik.chefsapp.model.AccessLevel;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.model.RecipeStep;
import com.majernik.chefsapp.presenter.CreateRecipePresenter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CreateRecipeActivity extends BaseCreateRecipeActivity implements CreateRecipeContract.View
{

    public static final int EDIT_RECIPE_REQUEST = 1;

    private ImageButton addIngredientButton, addStepButton;
    private FloatingActionButton cameraButton;

    private ImageView recipeImage;

    private EditText recipeTitle, recipeDescription;
    private Spinner recipeAccessLevel;

    private CreateRecipePresenter crPresenter;
    private IngredientsPopup ingredientsPopup;

    private IngredientListAdapter ingredientListAdapter;
    private RecipeStepsListAdapter recipeStepsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_recipe);

        AccessLevelAdapter<AccessLevel> adapter = new AccessLevelAdapter<AccessLevel>(
                getApplicationContext(),
                android.R.layout.simple_spinner_item,
                AccessLevel.values()
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        recipeTitle = findViewById(R.id.recipe_header);
        recipeDescription = findViewById(R.id.recipe_description);
        recipeAccessLevel = findViewById(R.id.recipe_access_level);
        recipeAccessLevel.setAdapter(adapter);

        Recipe recipe;
        if (getIntent().hasExtra("recipe")) {
            recipe = (Recipe) getIntent().getSerializableExtra("recipe");
            recipeTitle.setText(recipe.getTitle());
            recipeDescription.setText(recipe.getDescription());
            recipeAccessLevel.setSelection(adapter.getPosition(recipe.getAccessLevel()));
        } else {
            recipe = new Recipe();
        }

        crPresenter = new CreateRecipePresenter(this, recipe);

        ingredientsPopup = new IngredientsPopup(this, crPresenter);

        addIngredientButton = findViewById(R.id.button_add_ingredient);
        addIngredientButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ingredientsPopup.showPopup();
            }
        });
        addStepButton = findViewById(R.id.button_add_step);
        addStepButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showCreateRecipeStepActivity();
            }
        });
        cameraButton = findViewById(R.id.button_camera);
        cameraButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openCamera();
            }
        });

        initRecipeImage(recipe);
        initIngredientsList(recipe.getIngredients());
        initRecipeStepsList(recipe.getSteps());
    }

    @Override
    protected void userLoaded()
    {

    }

    @Override
    public void onBackPressed()
    {
        persistRecipe();
        setResultIntent(crPresenter.getRecipe());
        super.onBackPressed();
    }

    public IngredientsPopup getIngredientsPopup()
    {
        return ingredientsPopup;
    }

    @Override
    public void addIngredientToView(RecipeIngredient ingredient)
    {
        ingredientListAdapter.notifyItemInserted(ingredientListAdapter.getItemCount() + 1);
    }

    @Override
    public void removeIngredientFromView(RecipeIngredient ingredient)
    {
        ingredientListAdapter.notifyDataSetChanged();
    }

    private void showCreateRecipeStepActivity()
    {
        Intent intent = new Intent(this, CreateRecipeStepActivity.class);
        startActivityForResult(intent, CreateRecipeStepActivity.ADD_RECIPE_STEP_REQUEST);
    }

    public void startEditRecipeStepActivity(RecipeStep step)
    {
        Intent intent = new Intent(this, CreateRecipeStepActivity.class);
        intent.putExtra("recipe", crPresenter.getRecipe());
        intent.putExtra("recipe_step", step);
        startActivityForResult(intent, CreateRecipeStepActivity.UPDATE_RECIPE_STEP_REQUEST);
    }

    private void persistRecipe()
    {
        AccessLevel accessLevel = (AccessLevel) recipeAccessLevel.getSelectedItem();
        crPresenter.setRecipeTitle(recipeTitle.getText().toString());
        crPresenter.setRecipeDescription(recipeDescription.getText().toString());
        crPresenter.setRecipeAccessLevel(accessLevel.getValue());
        crPresenter.persistRecipe();
    }

    private void initRecipeImage(Recipe recipe)
    {
        recipeImage = findViewById(R.id.recipe_image);
        if (recipe.hasImage()) {
            Picasso.get().load(UrlUtility.getUrl(recipe.getImage())).into(recipeImage);
        }
    }

    /**
     * Initialize RecyclerView for ingredients
     *
     * @param ingredients
     */
    private void initIngredientsList(List<RecipeIngredient> ingredients)
    {
        ingredientListAdapter = new IngredientListAdapter(ingredients,
                new OnIngredientDeleteClickListener()
                {
                    @Override
                    public void onIngredientDeleteClick(RecipeIngredient ingredient)
                    {
                        crPresenter.removeIngredient(ingredient);
                    }
                });
        initRecyclerView(ingredientListAdapter, R.id.ingredients_list);
    }

    /**
     * Initialize RecyclerView for recipe steps
     *
     * @param steps
     */
    private void initRecipeStepsList(List<RecipeStep> steps)
    {
        recipeStepsListAdapter = new RecipeStepsListAdapter(steps, true,
                new OnRecipeStepThumbnailClickListener()
                {
                    @Override
                    public void onRecipeStepThumbnailClick(RecipeStep step)
                    {
                        openMediaPlayer(step);
                    }
                },
                new OnRecipeStepDeleteClickListener()
                {
                    @Override
                    public void onRecipeStepDeleteClick(RecipeStep step)
                    {
                        crPresenter.removeRecipeStep(step);
                    }
                },
                new OnRecipeStepEditClickListener()
                {
                    @Override
                    public void onRecipeStepEditClick(RecipeStep step)
                    {
                        startEditRecipeStepActivity(step);
                    }
                }
        );

        RecyclerView recyclerView = findViewById(R.id.recipe_steps_list);
        recyclerView.setAdapter(recipeStepsListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(recipeStepsListAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }

    /**
     * Initialize RecyclerView
     *
     * @param adapter
     * @param id
     */
    private void initRecyclerView(RecyclerView.Adapter adapter, int id)
    {
        RecyclerView recyclerView = findViewById(id);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    public void setResultIntent(Recipe recipe)
    {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("recipe", recipe);

        setResult(Activity.RESULT_OK, resultIntent);
    }

    @Override
    public void notifyStepAdded()
    {
        recipeStepsListAdapter.notifyItemInserted(recipeStepsListAdapter.getItemCount() + 1);
    }

    @Override
    public void notifyStepChanged(RecipeStep step)
    {
        recipeStepsListAdapter.setRecipeStep(step);
        recipeStepsListAdapter.notifyItemChanged(step.getPosition() - 1);
    }

    @Override
    public void notifyStepsChanged(List<RecipeStep> steps)
    {
        recipeStepsListAdapter.setRecipeSteps(steps);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CreateRecipeStepActivity.ADD_RECIPE_STEP_REQUEST
                    && data.hasExtra("recipe_step")) {
                RecipeStep step = (RecipeStep) data.getSerializableExtra("recipe_step");
                if (!step.isEmpty()) {
                    step.setPosition(recipeStepsListAdapter.getItemCount() + 1);
                    crPresenter.addRecipeStep(step);
                }
            } else if (requestCode == CreateRecipeStepActivity.UPDATE_RECIPE_STEP_REQUEST
                    && data.hasExtra("recipe_step")) {
                RecipeStep step = (RecipeStep) data.getSerializableExtra("recipe_step");
                if (!step.isEmpty()) {
                    crPresenter.updateRecipeStep(step);
                }
            } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                recipeImage.setImageBitmap(imageBitmap);
                crPresenter.uploadRecipeImage(imageBitmap);
            }
        }
    }
}
