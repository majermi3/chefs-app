package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.User;

public interface FriendshipListener
{
    public void removeFriendship(User user);
}
