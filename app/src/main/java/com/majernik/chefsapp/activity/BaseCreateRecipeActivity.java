package com.majernik.chefsapp.activity;

import android.content.Intent;
import android.provider.MediaStore;

import com.majernik.chefsapp.model.RecipeStep;

abstract public class BaseCreateRecipeActivity extends BaseActivity
{

    protected void openCamera()
    {
        openCamera(false);
    }

    /**
     * Opens Intent for taking a picture
     */
    protected void openCamera(boolean isVideo)
    {
        if (isVideo) {
            openVideoCamera();
        } else {
            openImageCamera(REQUEST_IMAGE_CAPTURE);
        }
    }

    protected void openMediaPlayer(RecipeStep step)
    {

    }

    private void openVideoCamera()
    {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_VIDEO_CAPTURE);
        }
    }
}
