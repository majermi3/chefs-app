package com.majernik.chefsapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.LayoutRes;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import de.hdodenhof.circleimageview.CircleImageView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.majernik.chefsapp.R;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.UrlUtility;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.api.response.UserResponse;
import com.majernik.chefsapp.model.User;
import com.majernik.chefsapp.service.UserService;
import com.squareup.picasso.Picasso;

import java.io.IOException;

@SuppressLint("Registered")
abstract public class BaseActivity extends AppCompatActivity
{
    public static final int REQUEST_PROFILE_IMAGE_CAPTURE = 45;
    public static final int REQUEST_IMAGE_CAPTURE = 50;
    public static final int REQUEST_VIDEO_CAPTURE = 55;

    private final int MENU_ITEM_DASHBOARD = 0;
    private final int MENU_ITEM_SHOPPING_CART = 1;
    private final int MENU_ITEM_FRIENDS = 2;
    private final int MENU_ITEM_SETTINGS = 3;
    private final int MENU_ITEM_LOGOUT = 4;

    protected DrawerLayout drawerLayout;
    protected UserService userService;

    protected User user;

    private NavigationView navView;
    private View hView;
    private MenuItem settingsMenuItem;

    private TextView userNameView;
    private CircleImageView profileImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        userService = new UserService(this);
        userService.getCurrentUser(new ApiServiceInterface.UserService.OnFinishedListener()
        {
            @Override
            public void onFinished(UserResponse userResponse)
            {
                user = userResponse.getUser();

                if (!user.hasFcmRefreshToken()) {
                    setFcmRefreshToken();
                }

                setNavigationUserName();

                if (user.hasImage() && profileImageView != null) {
                    Picasso.get().load(UrlUtility.getUrl(user.getImage())).into(profileImageView);
                }

                userLoaded();
            }

            @Override
            public void onFailure(Throwable t)
            {
                userNotLoaded(t);
            }
        });
    }

    private void setFcmRefreshToken()
    {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task)
                    {
                        if (!task.isSuccessful()) {
                            Log.w("TOKEN", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        user.setFcmRefreshToken(token);
                        userService.updateCurrentUser(user,
                                new ApiServiceInterface.OnFinishedListener()
                                {
                                    @Override
                                    public void onFinished(ApiResponse response)
                                    {
                                        Log.d("TOKEN", "Refreshed token set.");
                                    }

                                    @Override
                                    public void onFailure(Throwable t)
                                    {

                                        Log.d("TOKEN", "Token not set: ");
                                    }
                                });
                        // Log and toast
                        Log.d("TOKEN", token);
                    }
                });
    }

    abstract protected void userLoaded();

    protected void userNotLoaded(Throwable t)
    {
        Log.d("API ERROR", t.getMessage());
    }

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);
        onCreateDrawer();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        super.setContentView(layoutResID);
        onCreateDrawer();
    }

    protected void onCreateDrawer()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        navView = findViewById(R.id.nav_view);
        if (navView != null) {
            hView = navView.getHeaderView(0);
            userNameView = hView.findViewById(R.id.nav_header_user_name);
            initProfilePicture(hView);
            initMenu(navView.getMenu());
        }

        ActionBar actionbar = getSupportActionBar();
        actionbar.setTitle(R.string.app_name);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white);

        setNavigationUserName();
    }

    private void initProfilePicture(View headerView)
    {
        profileImageView = headerView.findViewById(R.id.profile_image);
        profileImageView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openImageCamera(REQUEST_PROFILE_IMAGE_CAPTURE);
            }
        });
        if (user != null && user.hasImage()) {
            Picasso.get().load(UrlUtility.getUrl(user.getImage())).into(profileImageView);
        }
    }

    private void initMenu(Menu menu)
    {
        menu.getItem(MENU_ITEM_DASHBOARD).setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem)
                    {
                        redirect(DashboardActivity.class);
                        return false;
                    }
                });
        menu.getItem(MENU_ITEM_SHOPPING_CART).setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem)
                    {
                        redirect(ShoppingCartActivity.class);
                        return false;
                    }
                });
        menu.getItem(MENU_ITEM_FRIENDS).setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem)
                    {
                        redirect(FriendsActivity.class);
                        return false;
                    }
                });
        menu.getItem(MENU_ITEM_SETTINGS).setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem)
                    {
                        redirect(SettingsActivity.class);
                        return false;
                    }
                });
        menu.getItem(MENU_ITEM_LOGOUT).setOnMenuItemClickListener(
                new MenuItem.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem)
                    {
                        userService.logout();
                        redirect(MainActivity.class);
                        return false;
                    }
                });
    }

    protected void redirectAndFinish(Class<?> cls)
    {
        Intent intent = new Intent(this, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    protected void redirect(Class<?> cls)
    {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    protected void redirectToFragment(Class<?> activityClass, Class<?> fragmentClass)
    {
        Intent intent = new Intent(this, activityClass);
        intent.putExtra("redirect", fragmentClass.toString());
        startActivity(intent);
    }

    private void setNavigationUserName()
    {
        if (user != null && userNameView != null) {
            userNameView.setText(user.getName());
        }
    }

    protected void openImageMediaPlayer(String url) throws IOException
    {
        openMediaPlayer(url, "image/*");
    }

    protected void openVideoMediaPlayer(String url) throws IOException
    {
        openMediaPlayer(url, "video/*");
    }

    protected void openMediaPlayer(String url, String dataType) throws IOException
    {
        Intent viewMediaIntent = new Intent();
        viewMediaIntent.setAction(android.content.Intent.ACTION_VIEW);
        viewMediaIntent.setDataAndType(Uri.parse(url), dataType);
        viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(viewMediaIntent);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu)
    {
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showToast(int resourceId)
    {
        showToast(getString(resourceId));
    }

    public void showToast(String message)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    protected void openImageCamera(int requestImage)
    {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, requestImage);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_PROFILE_IMAGE_CAPTURE) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                profileImageView.setImageBitmap(imageBitmap);
                try {
                    userService.uploadRecipeStepImage(imageBitmap,
                            new ApiServiceInterface.OnMediaUploadedListener()
                            {
                                @Override
                                public void onFinished(MediaResponse response)
                                {
                                    showToast(R.string.toast__profile__image_set);
                                }

                                @Override
                                public void onFailure(Throwable t)
                                {
                                    showToast(R.string.toast__profile__image_not_set);
                                }
                            });
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
