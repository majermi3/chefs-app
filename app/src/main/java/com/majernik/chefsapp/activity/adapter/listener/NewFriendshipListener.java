package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.User;

public interface NewFriendshipListener
{
    public void onFriendshipRequest(User user);
}
