package com.majernik.chefsapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.api.UrlUtility;
import com.majernik.chefsapp.contract.CreateRecipeStepContract;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeStep;
import com.majernik.chefsapp.presenter.CreateRecipeStepPresenter;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class CreateRecipeStepActivity extends BaseCreateRecipeActivity implements CreateRecipeStepContract.View
{

    private CreateRecipeStepPresenter mPresenter;

    public static final int ADD_RECIPE_STEP_REQUEST = 90;
    public static final int UPDATE_RECIPE_STEP_REQUEST = 95;

    private EditText descriptionEditText;
    private ImageView recipeStepMedia;
    private FloatingActionButton cameraButton, mediaSwitch;

    private Recipe recipe;
    private RecipeStep step;

    private boolean isVideoSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_recipe_step);

        descriptionEditText = findViewById(R.id.recipe_step_description);

        cameraButton = findViewById(R.id.button_camera);
        cameraButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openCamera(isVideoSelected);
            }
        });

        recipe = (Recipe) getIntent().getSerializableExtra("recipe");
        step = (RecipeStep) getIntent().getSerializableExtra("recipe_step");
        if (step == null) {
            step = new RecipeStep();
        } else {
            descriptionEditText.setText(step.getDescription());
        }

        mPresenter = new CreateRecipeStepPresenter(this, recipe, step);

        mediaSwitch = findViewById(R.id.media_switch);
        mediaSwitch.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                switchMediaImages();
            }
        });
        initRecipeStepImage(step);
    }

    @Override
    protected void userLoaded()
    {

    }

    @Override
    public void onBackPressed()
    {
        step.setDescription(descriptionEditText.getText().toString());
        setResultIntent(step);
        super.onBackPressed();
    }

    public void setResultIntent(RecipeStep step)
    {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("recipe_step", step);

        setResult(Activity.RESULT_OK, resultIntent);
    }

    private void switchMediaImages()
    {
        if (isVideoSelected) {
            mediaSwitch.setImageResource(R.drawable.ic_videocam_white_48dp);
            cameraButton.setImageResource(R.drawable.ic_camera_white_48dp);
        } else {
            mediaSwitch.setImageResource(R.drawable.ic_camera_white_48dp);
            cameraButton.setImageResource(R.drawable.ic_videocam_white_48dp);
        }
        isVideoSelected = !isVideoSelected;
    }

    private void initRecipeStepImage(RecipeStep recipeStep)
    {
        recipeStepMedia = findViewById(R.id.recipe_step_media);
        if (recipeStep.hasThumbnail()) {
            String url = UrlUtility.getUrl(recipeStep.getThumbnail());
            Picasso.get().load(url).into(recipeStepMedia);
        } else if (recipeStep.hasMedia()) {
            String url = UrlUtility.getUrl(recipeStep.getMedia());
            Picasso.get().load(url).into(recipeStepMedia);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bundle extras = intent.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                recipeStepMedia.setImageBitmap(imageBitmap);
                mPresenter.uploadRecipeStepImage(imageBitmap);
            } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
                try {
                    AssetFileDescriptor videoAsset = getContentResolver().openAssetFileDescriptor(
                            intent.getData(), "r");
                    Bitmap thumbnail = mPresenter.uploadRecipeStepVideo(videoAsset);
                    recipeStepMedia.setImageBitmap(thumbnail);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }
}
