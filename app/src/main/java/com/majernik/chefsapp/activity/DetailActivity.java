package com.majernik.chefsapp.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.IngredientListAdapter;
import com.majernik.chefsapp.activity.adapter.RecipeStepsListAdapter;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeStepThumbnailClickListener;
import com.majernik.chefsapp.activity.fragment.dashboard.MyRecipesFragment;
import com.majernik.chefsapp.api.UrlUtility;
import com.majernik.chefsapp.contract.DetailContract;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeStep;
import com.majernik.chefsapp.presenter.DetailPresenter;
import com.majernik.chefsapp.service.CartService;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class DetailActivity extends BaseActivity implements DetailContract.View
{
    RecyclerView ingredientsList, recipeStepsList;
    TextView title;
    ImageButton editButton, deleteButton, addToCartButton;

    ImageView recipeImage;

    protected Recipe recipe;

    private IngredientListAdapter ingredientListAdapter;
    private RecipeStepsListAdapter recipeStepsListAdapter;

    private DetailContract.Presenter presenter;

    private CartService cartService;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        cartService = new CartService(getApplicationContext());

        editButton = findViewById(R.id.button_recipe_edit);
        deleteButton = findViewById(R.id.button_recipe_delete);
        addToCartButton = findViewById(R.id.button_add_to_cart);
        recipe = (Recipe) getIntent().getSerializableExtra("recipe");
        ingredientsList = findViewById(R.id.ingredients_list);
        recipeStepsList = findViewById(R.id.recipe_steps_list);
        title = findViewById(R.id.recipe_title);

        recipeImage = findViewById(R.id.recipe_image);
        setRecipeImage();

        ingredientListAdapter = new IngredientListAdapter(recipe.getIngredients());
        ingredientsList.setAdapter(ingredientListAdapter);
        ingredientsList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        recipeStepsListAdapter = new RecipeStepsListAdapter(
                recipe.getSteps(),
                false,
                new OnRecipeStepThumbnailClickListener()
                {
                    @Override
                    public void onRecipeStepThumbnailClick(RecipeStep step)
                    {
                        openMediaPlayer(step);
                    }
                });
        recipeStepsList.setAdapter(recipeStepsListAdapter);
        recipeStepsList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        presenter = new DetailPresenter(this);

        addToCartButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                cartService.addRecipeToCart(recipe);
                showToast(R.string.toast__cart__ingredients_added);
            }
        });

        if (isEditable()) {
            editButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    startEditRecipeActivity();
                }
            });
            deleteButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    openAlertDialog();
                }
            });
        } else {
            editButton.setVisibility(View.INVISIBLE);
            deleteButton.setVisibility(View.INVISIBLE);
        }

        setElementValues();
    }

    @Override
    protected void userLoaded()
    {

    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CreateRecipeActivity.EDIT_RECIPE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data.hasExtra("recipe")) {
                recipe = (Recipe) data.getSerializableExtra("recipe");
                setElementValues();
                ingredientListAdapter.setIngredients(recipe.getIngredients());
                recipeStepsListAdapter.setRecipeSteps(recipe.getSteps());
                setRecipeImage();
                setResult(Activity.RESULT_OK, data);
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setRecipeImage()
    {
        if (recipe.hasImage()) {
            Picasso.get().load(UrlUtility.getUrl(recipe.getImage())).into(recipeImage);
        }
    }

    private void setElementValues()
    {
        if (recipe.hasTitle()) {
            title.setText(recipe.getTitle());
        }
    }

    private void openAlertDialog()
    {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_delete_title)
                .setMessage(R.string.dialog_delete_recipe_message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setNegativeButton(R.string.dialog_delete_no, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                    }
                })
                .setPositiveButton(R.string.dialog_delete_yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        presenter.deleteRecipe(recipe);
                    }
                })
                .create().show();
    }

    private void startEditRecipeActivity()
    {
        Intent intent = new Intent(getApplicationContext(), CreateRecipeActivity.class);
        intent.putExtra("recipe", recipe);
        startActivityForResult(intent, CreateRecipeActivity.EDIT_RECIPE_REQUEST);
    }

    protected void openMediaPlayer(RecipeStep step)
    {
        try {
            if (step.hasVideo()) {
                openVideoMediaPlayer(UrlUtility.getUrl(step.getMedia()));
            } else {
                openImageMediaPlayer(UrlUtility.getUrl(step.getMedia()));
            }
        } catch (IOException e) {
            showToast(R.string.toast__detail__media_not_loaded);
        }
    }

    /**
     * Determines whether user can edit recipe
     *
     * @return boolean
     */
    private boolean isEditable()
    {
        return getIntent().getBooleanExtra("isOwner", false);
    }

    @Override
    public void showMyRecipesView()
    {
        Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
        intent.putExtra("redirect", MyRecipesFragment.class.toString());
        startActivity(intent);
        finish();
    }
}
