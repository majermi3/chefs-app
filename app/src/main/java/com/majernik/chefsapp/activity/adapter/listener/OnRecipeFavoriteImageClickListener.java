package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.model.Recipe;

public interface OnRecipeFavoriteImageClickListener
{
    void onRecipeFavoriteMarked(Recipe recipe,
                                ApiServiceInterface.OnFinishedListener onFinishedListener);

    void onRecipeFavoriteUnmarked(Recipe recipe,
                                  ApiServiceInterface.OnFinishedListener onFinishedListener);
}
