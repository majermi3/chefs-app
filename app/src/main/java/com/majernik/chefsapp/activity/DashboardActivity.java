package com.majernik.chefsapp.activity;

import android.content.Intent;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.pager.DashboardPagerAdapter;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeFavoriteImageClickListener;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.contract.DashboardContract;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.presenter.DashboardPresenter;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends BaseActivity implements DashboardContract.View, OnRecipeFavoriteImageClickListener
{
    List<Recipe> recipes;
    DashboardPresenter dashboardPresenter;

    private ViewPager slideViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        recipes = new ArrayList<Recipe>();
        dashboardPresenter = new DashboardPresenter(this);

        setContentView(R.layout.activity_dashboard);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void userLoaded()
    {
        slideViewPager = findViewById(R.id.slideViewPager);
        DashboardPagerAdapter pageAdapter = new DashboardPagerAdapter(getSupportFragmentManager(),
                2);
        slideViewPager.setAdapter(pageAdapter);
        setUpTabs();

        if (getIntent().hasExtra("redirect")) {
            slideViewPager.setCurrentItem(
                    pageAdapter.getFragmentPosition(getIntent().getStringExtra("redirect")));
        }
    }


    @Override
    public void addRecipes(List<Recipe> recipes)
    {
        recipes.addAll(recipes);
    }

    @Override
    public void showError(String message)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }

    public void redirectToLogin()
    {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRecipeFavoriteMarked(Recipe recipe,
                                       ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        dashboardPresenter.markRecipeAsFavorite(recipe, onFinishedListener);
    }

    @Override
    public void onRecipeFavoriteUnmarked(Recipe recipe,
                                         ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        dashboardPresenter.unmarkRecipeAsFavorite(recipe, onFinishedListener);
    }

    private void setUpTabs()
    {
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(slideViewPager);
        tabs.getTabAt(0).setIcon(R.drawable.ic_dashboard_white_24dp);
        tabs.getTabAt(1).setIcon(R.drawable.pot_white);
    }
}
