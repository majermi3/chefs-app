package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.RecipeStep;

public interface OnRecipeStepDeleteClickListener
{
    void onRecipeStepDeleteClick(RecipeStep step);
}
