package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.RecipeStep;

public interface OnRecipeStepThumbnailClickListener
{
    void onRecipeStepThumbnailClick(RecipeStep step);
}
