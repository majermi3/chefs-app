package com.majernik.chefsapp.activity.fragment.friends;

import android.view.View;

import com.majernik.chefsapp.activity.adapter.UserListAdapter;
import com.majernik.chefsapp.activity.fragment.BaseFragment;

import androidx.recyclerview.widget.RecyclerView;

abstract public class BaseFriendsFragment extends BaseFragment
{
    protected View view;
    protected RecyclerView userList;
    protected UserListAdapter userListAdapter;
}
