package com.majernik.chefsapp.activity.adapter.pager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.majernik.chefsapp.activity.fragment.dashboard.DashboardFragment;
import com.majernik.chefsapp.activity.fragment.dashboard.MyRecipesFragment;

public class DashboardPagerAdapter extends FragmentStatePagerAdapter
{
    private int numOfPages;

    public DashboardPagerAdapter(FragmentManager fm, int numOfPages)
    {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.numOfPages = numOfPages;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position) {
            case 0:
                return DashboardFragment.newInstance();
            case 1:
                return MyRecipesFragment.newInstance();
            default:
                return null;
        }
    }

    public int getFragmentPosition(String className)
    {
        if (className.equals(MyRecipesFragment.class.toString())) {
            return 1;
        }
        return 0;
    }

    @Override
    public int getCount()
    {
        return numOfPages;
    }
}
