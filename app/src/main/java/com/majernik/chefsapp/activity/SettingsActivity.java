package com.majernik.chefsapp.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.model.Settings;
import com.majernik.chefsapp.service.SettingsService;

public class SettingsActivity extends BaseActivity
{

    private SettingsService settingsService;
    private Settings settings;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        settingsService = new SettingsService(getApplicationContext());
        settings = settingsService.loadSettings();
        initializeUnitsSpinner();
    }

    @Override
    protected void userLoaded()
    {

    }

    private void initializeUnitsSpinner()
    {
        Spinner unitsSpinner = findViewById(R.id.unit_system);
        unitsSpinner.setAdapter(getUnitsSpinnerArrayAdapter());
        unitsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                settings.setUnitsSystem(position);
                settingsService.saveSettings(settings);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        unitsSpinner.setSelection(settings.getUnitsSystem());
    }

    private ArrayAdapter<String> getUnitsSpinnerArrayAdapter()
    {
        String[] arraySpinner = new String[]{
                getString(R.string.label_unit_system_metric),
                getString(R.string.label_unit_system_imperial)
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }
}
