package com.majernik.chefsapp.activity.adapter.listener;

public interface AdapterMoveListener
{
    void onItemMove(int fromPosition, int toPosition);

    void onItemDrop(int fromPosition, int toPosition);
}
