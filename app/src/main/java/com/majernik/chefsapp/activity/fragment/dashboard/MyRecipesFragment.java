package com.majernik.chefsapp.activity.fragment.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.CreateRecipeActivity;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeImageClickListener;
import com.majernik.chefsapp.activity.adapter.RecipeListAdapter;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.RecipesResponse;
import com.majernik.chefsapp.model.Recipe;

public class MyRecipesFragment extends BaseDashboardFragment
{
    private FloatingActionButton createRecipeButton;

    private RecipeListAdapter recipeListAdapter;

    View view;

    public MyRecipesFragment()
    {

    }

    public static MyRecipesFragment newInstance()
    {
        return new MyRecipesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_my_recipes, container, false);

        if (user != null) {
            initializeRecipes();
        }

        return view;
    }

    @Override
    protected void userLoaded()
    {
        if (recipeListAdapter == null && view != null) {
            initializeRecipes();
        }
    }

    private void initializeRecipes()
    {
        recipeListAdapter = new RecipeListAdapter(
                recipes,
                user,
                new OnRecipeImageClickListener()
                {
                    @Override
                    public void onRecipeImageClick(Recipe recipe)
                    {
                        showDetailActivity(recipe, true);
                    }
                },
                onRecipeFavoriteImageClickListener
        );

        RecyclerView recyclerView = view.findViewById(R.id.my_recipes_list);
        recyclerView.setAdapter(recipeListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        loadMyRecipes();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        createRecipeButton = getView().findViewById(R.id.button_create_recipe);
        createRecipeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getContext(), CreateRecipeActivity.class);
                startActivityForResult(intent, CreateRecipeActivity.EDIT_RECIPE_REQUEST);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CreateRecipeActivity.EDIT_RECIPE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data.hasExtra("recipe")) {
                Recipe recipe = (Recipe) data.getSerializableExtra("recipe");
                for (int i = 0; i < recipes.size(); i++) {
                    if (recipes.get(i).getId().equals(recipe.getId())) {
                        recipes.set(i, recipe);
                        recipeListAdapter.notifyItemChanged(i);
                        return;
                    }
                }
                recipes.add(recipe);
                recipeListAdapter.notifyItemInserted(recipes.size() - 1);
            }
        }
    }

    private void loadMyRecipes()
    {
        recipeService.getMyRecipes(new ApiServiceInterface.RecipeService.OnFinishedListener()
        {
            @Override
            public void onFinished(RecipesResponse recipesResponse)
            {
                if (recipesResponse.isSuccessful()) {
                    recipeListAdapter.update(recipesResponse.getRecipes());
                }
            }

            @Override
            public void onFailure(Throwable t)
            {
                System.out.println(t.getMessage());
            }
        });
    }
}
