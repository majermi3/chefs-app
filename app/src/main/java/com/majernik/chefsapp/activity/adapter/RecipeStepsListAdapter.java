package com.majernik.chefsapp.activity.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.listener.AdapterMoveListener;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeStepDeleteClickListener;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeStepEditClickListener;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeStepThumbnailClickListener;
import com.majernik.chefsapp.api.UrlUtility;
import com.majernik.chefsapp.model.RecipeStep;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class RecipeStepsListAdapter extends RecyclerView.Adapter<RecipeStepsListAdapter.ViewHolder> implements AdapterMoveListener
{
    private List<RecipeStep> steps;
    private OnRecipeStepThumbnailClickListener thumbnailClickListener;
    private OnRecipeStepDeleteClickListener deleteClickListener;
    private OnRecipeStepEditClickListener editClickListener;
    private View view;
    private boolean collapsed;

    public RecipeStepsListAdapter(List<RecipeStep> steps, boolean collapsed,
                                  OnRecipeStepThumbnailClickListener thumbnailClickListener)
    {
        this.steps = steps;
        this.collapsed = collapsed;
        this.thumbnailClickListener = thumbnailClickListener;
    }

    public RecipeStepsListAdapter(List<RecipeStep> steps, boolean collapsed,
                                  OnRecipeStepThumbnailClickListener thumbnailClickListener,
                                  OnRecipeStepDeleteClickListener deleteClickListener,
                                  OnRecipeStepEditClickListener editClickListener)
    {
        this.steps = steps;
        this.collapsed = collapsed;
        this.thumbnailClickListener = thumbnailClickListener;
        this.deleteClickListener = deleteClickListener;
        this.editClickListener = editClickListener;
    }

    @NonNull
    @Override
    public RecipeStepsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.list_recipe_steps_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeStepsListAdapter.ViewHolder holder, int position)
    {
        RecipeStep step = steps.get(position);
        holder.setDescription(step.getDescription());
        holder.setPosition(step.getPosition());
        holder.setThumbnailImage(step);

        if (collapsed) {
            holder.collapse();
        }

        if (deleteClickListener != null) {
            holder.bindOnDeleteButtonClick(step, deleteClickListener);
            holder.bindOnEditButtonClick(step, editClickListener);
        } else {
            holder.hideButtons();
        }
        holder.bindOnThumbnailClick(step, thumbnailClickListener);
    }

    @Override
    public int getItemCount()
    {
        return steps.size();
    }

    public void setRecipeSteps(List<RecipeStep> steps)
    {
        this.steps = steps;
        this.notifyDataSetChanged();
    }

    public void setRecipeStep(RecipeStep step)
    {
        if (steps.size() >= step.getPosition()) {
            steps.set(step.getPosition() - 1, step);
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition)
    {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                steps.get(i).setPosition(i + 2);
                steps.get(i + 1).setPosition(i + 1);
                Collections.swap(steps, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                steps.get(i).setPosition(i);
                steps.get(i - 1).setPosition(i + 1);
                Collections.swap(steps, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDrop(int fromPosition, int toPosition)
    {
        if (fromPosition < toPosition) {
            notifyItemRangeChanged(fromPosition, toPosition + 1);
        } else {
            notifyItemRangeChanged(toPosition, fromPosition + 1);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView descriptionTextView, positionTextView;
        ImageButton deleteButton, editButton;
        ImageView thumbnailImage;

        public ViewHolder(View itemView)
        {
            super(itemView);
            descriptionTextView = itemView.findViewById(R.id.recipe_step_description);
            positionTextView = itemView.findViewById(R.id.recipe_step_position);
            deleteButton = itemView.findViewById(R.id.button_delete_recipe_step);
            editButton = itemView.findViewById(R.id.button_edit_recipe_step);
            thumbnailImage = itemView.findViewById(R.id.recipe_step_thumbnail);
        }

        public void collapse()
        {
            descriptionTextView.setMaxLines(3);
        }

        public void setDescription(String description)
        {
            descriptionTextView.setText(description);
        }

        public void setPosition(int position)
        {
            positionTextView.setText(position + "");
        }

        public void setThumbnailImage(RecipeStep step)
        {
            if (step.hasVideo()) {
                Picasso.get().load(UrlUtility.getUrl(step.getThumbnail())).into(thumbnailImage);
            } else if (step.hasImage()) {
                Picasso.get().load(UrlUtility.getUrl(step.getMedia())).into(thumbnailImage);
            } else {
                thumbnailImage.setVisibility(View.GONE);
            }
        }

        public void bindOnDeleteButtonClick(final RecipeStep step,
                                            final OnRecipeStepDeleteClickListener deleteClickListener)
        {
            deleteButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    deleteClickListener.onRecipeStepDeleteClick(step);
                }
            });
        }

        public void bindOnEditButtonClick(final RecipeStep step,
                                          final OnRecipeStepEditClickListener editClickListener)
        {
            editButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    editClickListener.onRecipeStepEditClick(step);
                }
            });
        }

        public void bindOnThumbnailClick(final RecipeStep step,
                                         final OnRecipeStepThumbnailClickListener thumbnailClickListener)
        {
            thumbnailImage.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    thumbnailClickListener.onRecipeStepThumbnailClick(step);
                }
            });
        }

        public void hideButtons()
        {
            hideDeleteButton();
            hideEditButton();
        }

        public void hideDeleteButton()
        {
            deleteButton.setVisibility(View.INVISIBLE);
        }

        public void hideEditButton()
        {
            editButton.setVisibility(View.INVISIBLE);
        }


    }
}
