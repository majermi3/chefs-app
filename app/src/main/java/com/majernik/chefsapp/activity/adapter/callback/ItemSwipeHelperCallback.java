package com.majernik.chefsapp.activity.adapter.callback;

import android.graphics.Canvas;
import android.view.View;

import com.majernik.chefsapp.activity.adapter.ShoppingListAdapter;
import com.majernik.chefsapp.activity.adapter.listener.AdapterSwipeLister;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class ItemSwipeHelperCallback  extends ItemTouchHelper.SimpleCallback
{
    private AdapterSwipeLister swipeLister;

    public ItemSwipeHelperCallback(int dragDirs, int swipeDirs, AdapterSwipeLister swipeLister)
    {
        super(dragDirs, swipeDirs);
        this.swipeLister = swipeLister;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
    {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
    {
        if (swipeLister != null) {
            swipeLister.onSwipe(viewHolder, direction, viewHolder.getAdapterPosition());
        }
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder)
    {
        View foregroundView = ((ShoppingListAdapter.ViewHolder) viewHolder).foregroundView;
        getDefaultUIUtil().clearView(foregroundView);
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState)
    {
        if (viewHolder != null) {
            View foregroundView = ((ShoppingListAdapter.ViewHolder) viewHolder).foregroundView;
            getDefaultUIUtil().onSelected(foregroundView);
        }

        super.onSelectedChanged(viewHolder, actionState);
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
    {
        View foregroundView = ((ShoppingListAdapter.ViewHolder) viewHolder).foregroundView;
        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);
    }

    @Override
    public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
    {
        View foregroundView = ((ShoppingListAdapter.ViewHolder) viewHolder).foregroundView;
        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);
    }
}
