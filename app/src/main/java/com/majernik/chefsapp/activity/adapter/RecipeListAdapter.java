package com.majernik.chefsapp.activity.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeFavoriteImageClickListener;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeImageClickListener;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.UrlUtility;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.ViewHolder>
{
    private ArrayList<Recipe> recipes;
    private User user;
    private OnRecipeImageClickListener onRecipeImageClickListener;
    private OnRecipeFavoriteImageClickListener onRecipeFavoriteImageClickListener;

    public RecipeListAdapter(ArrayList<Recipe> recipes,
                             User user,
                             OnRecipeImageClickListener onRecipeImageClickListener,
                             OnRecipeFavoriteImageClickListener onRecipeFavoriteImageClickListener)
    {
        this.recipes = recipes;
        this.user = user;
        this.onRecipeImageClickListener = onRecipeImageClickListener;
        this.onRecipeFavoriteImageClickListener = onRecipeFavoriteImageClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View recipeListView = inflater.inflate(R.layout.list_recipes_item, parent, false);

        return new RecipeListAdapter.ViewHolder(recipeListView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Recipe recipe = recipes.get(position);
        holder.title.setText(recipe.hasTitle() ? recipe.getTitle() : "No title");
        holder.setRecipeImage(recipe.getImage());
        if (user.hasFavorteRecipe(recipe)) {
            holder.markAsFavorite();
        }
        holder.bindOnImageClick(recipe, onRecipeImageClickListener);
        holder.bindOnFavoriteImageClick(recipe, onRecipeFavoriteImageClickListener);
    }

    @Override
    public int getItemCount()
    {
        return recipes.size();
    }

    public void update(List<Recipe> recipes)
    {
        this.recipes.addAll(recipes);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        boolean isMarkedAsFavorite = false;

        TextView title;
        ImageView image;
        ImageButton buttonFavoriteRecipe;

        public ViewHolder(View itemView)
        {
            super(itemView);

            title = itemView.findViewById(R.id.recipe_header);
            image = itemView.findViewById(R.id.recipe_image);
            buttonFavoriteRecipe = itemView.findViewById(R.id.button_recipe_favorite);
        }

        public void setRecipeImage(String imageUrl)
        {
            if (imageUrl != null && !imageUrl.isEmpty()) {
                Picasso.get().load(UrlUtility.getUrl(imageUrl))
                        .placeholder(R.drawable.pot)
                        .error(R.drawable.pot)
                        .into(image);
            } else {
                image.setImageResource(R.drawable.pot);
            }
        }

        public void bindOnImageClick(final Recipe recipe,
                                     final OnRecipeImageClickListener onRecipeImageClickListener)
        {
            image.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    onRecipeImageClickListener.onRecipeImageClick(recipe);
                }
            });
        }

        public void bindOnFavoriteImageClick(final Recipe recipe,
                                             final OnRecipeFavoriteImageClickListener onRecipeFavoriteImageClickListener)
        {
            buttonFavoriteRecipe.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(final View view)
                {
                    if (isMarkedAsFavorite) {
                        onRecipeFavoriteImageClickListener.onRecipeFavoriteUnmarked(recipe,
                                new ApiServiceInterface.OnFinishedListener()
                                {
                                    @Override
                                    public void onFinished(ApiResponse response)
                                    {
                                        unmarkAsFavorite();
                                    }

                                    @Override
                                    public void onFailure(Throwable t)
                                    {

                                    }
                                });
                    } else {
                        onRecipeFavoriteImageClickListener.onRecipeFavoriteMarked(recipe,
                                new ApiServiceInterface.OnFinishedListener()
                                {
                                    @Override
                                    public void onFinished(ApiResponse response)
                                    {
                                        markAsFavorite();
                                    }

                                    @Override
                                    public void onFailure(Throwable t)
                                    {

                                    }
                                });
                    }
                }
            });
        }

        public void markAsFavorite()
        {
            buttonFavoriteRecipe.setColorFilter(itemView.getResources().getColor(R.color.gold));
            isMarkedAsFavorite = true;
        }

        public void unmarkAsFavorite()
        {
            buttonFavoriteRecipe.setColorFilter(itemView.getResources().getColor(R.color.gray));
            isMarkedAsFavorite = false;
        }
    }
}
