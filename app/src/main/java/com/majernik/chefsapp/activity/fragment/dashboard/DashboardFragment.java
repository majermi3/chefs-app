package com.majernik.chefsapp.activity.fragment.dashboard;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeImageClickListener;
import com.majernik.chefsapp.activity.adapter.RecipeListAdapter;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.RecipesResponse;
import com.majernik.chefsapp.model.Recipe;

public class DashboardFragment extends BaseDashboardFragment
{

    private RecipeListAdapter recipeListAdapter;

    private View view;

    public DashboardFragment()
    {

    }

    /**
     * @return A new instance of fragment DashboardFragment.
     */
    public static DashboardFragment newInstance()
    {
        return new DashboardFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        if (user != null) {
            initializeRecipes();
        }

        return view;
    }

    @Override
    protected void userLoaded()
    {
        if (recipeListAdapter == null && view != null) {
            initializeRecipes();
        }
    }

    private void initializeRecipes()
    {
        recipeListAdapter = new RecipeListAdapter(
                recipes,
                user,
                new OnRecipeImageClickListener()
                {
                    @Override
                    public void onRecipeImageClick(Recipe recipe)
                    {
                        showDetailActivity(recipe, false);
                    }
                },
                onRecipeFavoriteImageClickListener
        );

        RecyclerView recyclerView = view.findViewById(R.id.my_recipes_list);
        recyclerView.setAdapter(recipeListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        loadRecipes();
    }

    private void loadRecipes()
    {
        recipeService.getRecipes(new ApiServiceInterface.RecipeService.OnFinishedListener()
        {
            @Override
            public void onFinished(RecipesResponse recipesResponse)
            {
                if (recipesResponse.isSuccessful()) {
                    recipeListAdapter.update(recipesResponse.getRecipes());
                }
            }

            @Override
            public void onFailure(Throwable t)
            {
                System.out.println(t.getMessage());
            }
        });
    }
}
