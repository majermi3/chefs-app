package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.Ingredient;

public interface OnIngredientClickListener
{
    void onIngredientClick(Ingredient ingredient);
}
