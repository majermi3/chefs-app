package com.majernik.chefsapp.activity;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.contract.LoginContract;
import com.majernik.chefsapp.presenter.LoginPresenter;

public class LoginActivity extends BaseActivity implements LoginContract.View, View.OnClickListener
{
    private EditText email, password;
    private Button loginBtn;

    LoginPresenter lPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        lPresenter = new LoginPresenter(this);

        email = this.findViewById(R.id.input_email);
        password = this.findViewById(R.id.input_password);
        loginBtn = this.findViewById(R.id.button_login);
        loginBtn.setOnClickListener(this);
    }

    @Override
    protected void userLoaded()
    {

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.button_login:
                lPresenter.loginUser(
                        email.getText().toString().trim(),
                        password.getText().toString().trim()
                );
                break;
        }
    }

    @Override
    public void showDashboard()
    {
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }

    @Override
    public void showError(String message)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.show();
    }
}
