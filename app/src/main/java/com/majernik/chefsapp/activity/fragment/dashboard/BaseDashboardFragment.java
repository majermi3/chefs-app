package com.majernik.chefsapp.activity.fragment.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.majernik.chefsapp.activity.CreateRecipeActivity;
import com.majernik.chefsapp.activity.DetailActivity;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeFavoriteImageClickListener;
import com.majernik.chefsapp.activity.fragment.BaseFragment;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.service.RecipeService;

import java.util.ArrayList;

abstract public class BaseDashboardFragment extends BaseFragment
{

    protected OnRecipeFavoriteImageClickListener onRecipeFavoriteImageClickListener;

    protected ArrayList<Recipe> recipes;

    protected RecipeService recipeService;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        recipes = new ArrayList<>();
    }

    abstract protected void userLoaded();

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        recipeService = new RecipeService(context);

        if (context instanceof OnRecipeFavoriteImageClickListener) {
            onRecipeFavoriteImageClickListener = (OnRecipeFavoriteImageClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRecipeFavoriteImageClickListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        onRecipeFavoriteImageClickListener = null;
    }

    protected void showDetailActivity(Recipe recipe, boolean isOwner)
    {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra("recipe", recipe);
        intent.putExtra("isOwner", isOwner);
        startActivityForResult(intent, CreateRecipeActivity.EDIT_RECIPE_REQUEST);
    }
}
