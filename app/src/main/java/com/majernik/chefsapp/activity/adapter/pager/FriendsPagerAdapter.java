package com.majernik.chefsapp.activity.adapter.pager;

import com.majernik.chefsapp.activity.fragment.friends.FriendsFragment;
import com.majernik.chefsapp.activity.fragment.friends.PendingRequestsFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FriendsPagerAdapter extends FragmentStatePagerAdapter
{
    private int numOfPages;

    public FriendsPagerAdapter(FragmentManager fm, int numOfPages)
    {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.numOfPages = numOfPages;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position) {
            case 0:
                return FriendsFragment.newInstance();
            case 1:
                return PendingRequestsFragment.newInstance();
            default:
                return null;
        }
    }

    public int getFragmentPosition(String fragmentClass)
    {
        if (fragmentClass.equals(PendingRequestsFragment.class.toString())) {
            return 1;
        }
        return 0;
    }

    @Override
    public int getCount()
    {
        return numOfPages;
    }
}
