package com.majernik.chefsapp.activity.popup;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.Switch;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.contract.CreateRecipeContract;
import com.majernik.chefsapp.model.Ingredient;
import com.majernik.chefsapp.activity.adapter.IngredientAdapter;
import com.majernik.chefsapp.activity.adapter.listener.OnIngredientClickListener;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.presenter.CreateRecipePresenter;
import com.majernik.chefsapp.service.SettingsService;
import com.majernik.chefsapp.utility.UnitsUtility;

import java.util.ArrayList;
import java.util.TreeMap;

public class IngredientsPopup implements CreateRecipeContract.Popup
{

    private ConstraintLayout mConstraintLayout;

    private AppCompatActivity activity;
    private CreateRecipePresenter presenter;
    private PopupWindow mPopupWindow;

    private EditText popupIngredientSearchField, ingredientAmount;
    private Switch unitSwitch;
    private NumberPicker unitPicker;
    private IngredientAdapter ingredientsAdapter;

    private SettingsService settingsService;

    public IngredientsPopup(AppCompatActivity activity, CreateRecipePresenter presenter)
    {
        this.activity = activity;
        this.presenter = presenter;

        settingsService = new SettingsService(activity.getApplicationContext());
        mConstraintLayout = activity.findViewById(R.id.layout_create_recipe);
    }

    @Override
    public void showIngredients(TreeMap<Integer, Ingredient> ingredients)
    {
        ingredientsAdapter.update(new ArrayList<>(ingredients.values()));
    }

    public void showPopup()
    {
        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_recipe_ingredient, null);

        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = (int) Math.round(dm.widthPixels * 0.9);
        int height = (int) Math.round(dm.heightPixels * 0.9);

        mPopupWindow = new PopupWindow(popupView, width, height);
        mPopupWindow.setFocusable(true);
        mPopupWindow.update();

        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener()
        {
            @Override
            public void onDismiss()
            {
                clearDim();
            }
        });

        popupIngredientSearchField = popupView.findViewById(R.id.find_ingredient);
        ingredientAmount = popupView.findViewById(R.id.ingredient_amount);
        initSearchField(popupView);
        initUnitPicker(popupView);
        initUnitSwitch(popupView);
        initAddButton(popupView);

        applyDim(0.5f);


        mPopupWindow.showAtLocation(mConstraintLayout, Gravity.CENTER, 0, 0);
    }

    /**
     * Initialize ingredient search field with text change watcher
     *
     * @param popupView Popup view
     */
    private void initSearchField(View popupView)
    {
        ingredientsAdapter = new IngredientAdapter(new ArrayList<Ingredient>(),
                new OnIngredientClickListener()
                {
                    @Override
                    public void onIngredientClick(Ingredient ingredient)
                    {
                        setUnitPickerData(getUnits(activity.getResources(), ingredient.isLiquid()));
                        unitSwitch.setVisibility(View.INVISIBLE);
                    }
                });
        RecyclerView recyclerView = popupView.findViewById(R.id.searched_ingredients_list);
        recyclerView.setAdapter(ingredientsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(popupView.getContext()));

        popupIngredientSearchField.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (s.length() > 0 && ingredientsAdapter.getSelected() == null) {
                    unitSwitch.setVisibility(View.VISIBLE);
                } else {
                    unitSwitch.setVisibility(View.INVISIBLE);
                }
                presenter.findIngredients(s);
            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });
    }

    private void initUnitPicker(View popupView)
    {
        String[] units = UnitsUtility.getUnitNames(activity.getResources(),
                settingsService.loadSettings(), false);
        unitPicker = popupView.findViewById(R.id.spinner_units);
        setUnitPickerData(units);
    }

    private void initUnitSwitch(final View popupView)
    {
        unitSwitch = popupView.findViewById(R.id.switch_units);
        unitSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
            {
                setUnitPickerData(getUnits(popupView.getResources(), isChecked));
            }
        });
    }

    /**
     * Get string unit representation
     *
     * @param resources
     * @param showVolume
     * @return
     */
    private String[] getUnits(Resources resources, boolean showVolume)
    {
        return UnitsUtility.getUnitNames(
                activity.getResources(),
                settingsService.loadSettings(),
                showVolume
        );
    }

    private void setUnitPickerData(String[] units)
    {
        unitPicker.setDisplayedValues(null);
        unitPicker.setMinValue(0);
        unitPicker.setMaxValue(units.length - 1);
        unitPicker.setDisplayedValues(units);
    }

    private void initAddButton(final View popupView)
    {
        ImageButton addPopupButton = popupView.findViewById(R.id.button_add_ingredient_popup);
        addPopupButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Ingredient selectedIngredient = ingredientsAdapter.getSelected();
                RecipeIngredient recipeIngredient = new RecipeIngredient();

                if (selectedIngredient == null) {
                    selectedIngredient = new Ingredient(
                            popupIngredientSearchField.getText().toString().trim());
                    selectedIngredient.setLiquid(unitSwitch.isChecked());
                }
                recipeIngredient.setIngredient(selectedIngredient);
                recipeIngredient.setAmount(
                        Double.parseDouble(ingredientAmount.getText().toString()));
                recipeIngredient.setUnit(unitPicker.getValue());
                recipeIngredient.setMetricUnitSystem(
                        settingsService.loadSettings().isMetricSystem());

                presenter.createIngredient(recipeIngredient);
                mPopupWindow.dismiss();
            }
        });
    }

    private void applyDim(float dimAmount)
    {
        ViewGroup rootView = (ViewGroup) activity.getWindow().getDecorView().getRootView();
        Drawable dim = new ColorDrawable(Color.BLACK);
        dim.setBounds(0, 0, rootView.getWidth(), rootView.getHeight());
        dim.setAlpha((int) (255 * dimAmount));

        ViewGroupOverlay overlay = rootView.getOverlay();
        overlay.add(dim);
    }

    private void clearDim()
    {
        ViewGroup rootView = (ViewGroup) activity.getWindow().getDecorView().getRootView();
        ViewGroupOverlay overlay = rootView.getOverlay();
        overlay.clear();
    }
}
