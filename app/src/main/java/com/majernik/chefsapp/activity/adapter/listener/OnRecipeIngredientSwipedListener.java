package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.RecipeIngredient;

public interface OnRecipeIngredientSwipedListener
{
    public void recipeIngredientSwiped(RecipeIngredient recipeIngredient);
}
