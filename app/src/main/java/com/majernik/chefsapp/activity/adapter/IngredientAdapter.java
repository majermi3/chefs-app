package com.majernik.chefsapp.activity.adapter;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.listener.OnIngredientClickListener;
import com.majernik.chefsapp.model.Ingredient;

import android.content.Context;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.ViewHolder>
{
    private ArrayList<Ingredient> ingredients;
    private OnIngredientClickListener onIngredientClickListener;

    private int selectedPos = RecyclerView.NO_POSITION;
    private String selectedIngredientId;

    public IngredientAdapter(ArrayList<Ingredient> ingredients,
                             OnIngredientClickListener onIngredientClickListener)
    {
        this.ingredients = ingredients;
        this.onIngredientClickListener = onIngredientClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View buttonIngredientListView = inflater.inflate(
                R.layout.button_list_ingredients, parent, false);

        return new ViewHolder(buttonIngredientListView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        Ingredient ingredient = ingredients.get(position);

        holder.bind(ingredients.get(position), onIngredientClickListener);
        TextView button = holder.messageButton;

        if (ingredient.getId().equals(selectedIngredientId)) {
            button.setBackgroundColor(
                    holder.itemView.getResources().getColor(R.color.colorPrimary));
            button.setTextColor(Color.WHITE);
        } else {
            selectedPos = RecyclerView.NO_POSITION;
            button.setBackgroundColor(Color.WHITE);
            button.setTextColor(Color.BLACK);
        }

        button.setText(ingredient.getTitle());
    }

    @Override
    public int getItemCount()
    {
        return ingredients.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView messageButton;

        public ViewHolder(View itemView)
        {
            super(itemView);
            messageButton = itemView.findViewById(R.id.ingredient_title);
        }

        public void bind(final Ingredient ingredient, final OnIngredientClickListener listener)
        {
            messageButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    notifyItemChanged(selectedPos);
                    selectedPos = getLayoutPosition();
                    selectedIngredientId = ingredients.get(selectedPos).getId();
                    notifyItemChanged(selectedPos);
                    listener.onIngredientClick(ingredient);
                }
            });
        }
    }

    /**
     * Returns selected ingredient
     *
     * @return Ingredient
     */
    public Ingredient getSelected()
    {
        if (selectedPos == RecyclerView.NO_POSITION) {
            return null;
        }
        return ingredients.get(selectedPos);
    }

    public void update(ArrayList<Ingredient> data)
    {
        ingredients.clear();
        ingredients.addAll(data);
        notifyDataSetChanged();
    }
}
