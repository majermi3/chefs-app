package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.User;

public interface PendingFriendshipListener
{
    public void acceptPendingFriendship(User user);

    public void rejectPendingFriendship(User user);
}
