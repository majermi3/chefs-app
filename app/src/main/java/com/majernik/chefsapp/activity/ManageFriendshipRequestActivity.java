package com.majernik.chefsapp.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ManageFriendshipRequestActivity extends BaseActivity
{
    private Button acceptButton, rejectButton;
    private TextView nameTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_manage_friendship_request);

        acceptButton = findViewById(R.id.button_accept_friendship);
        rejectButton = findViewById(R.id.button_reject_friendship);
        nameTextView = findViewById(R.id.user_name);

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void userLoaded()
    {
        try {
            JSONObject friend = new JSONObject(getIntent().getStringExtra("friend"));
            final String friendId = friend.getString("id");

            nameTextView.setText(friend.getString("name"));

            acceptButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    userService.acceptFriendshipRequest(friendId,
                            new ApiServiceInterface.OnFinishedListener()
                            {
                                @Override
                                public void onFinished(ApiResponse response)
                                {
                                    showToast(R.string.toast__friends__request_accepted);
                                    redirectAndFinish(FriendsActivity.class);
                                }

                                @Override
                                public void onFailure(Throwable t)
                                {
                                    showToast(
                                            R.string.toast__friends__request_could_not_be_accepted);
                                    redirectAndFinish(FriendsActivity.class);
                                }
                            });
                }
            });
            rejectButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    userService.rejectFriendshipRequest(friendId,
                            new ApiServiceInterface.OnFinishedListener()
                            {
                                @Override
                                public void onFinished(ApiResponse response)
                                {
                                    showToast(R.string.toast__friends__request_rejected);
                                    redirectAndFinish(FriendsActivity.class);
                                }

                                @Override
                                public void onFailure(Throwable t)
                                {
                                    showToast(
                                            R.string.toast__friends__request_could_not_be_rejected);
                                    redirectAndFinish(FriendsActivity.class);
                                }
                            });
                }
            });
        } catch (JSONException e) {
            showToast(R.string.toast__friends__user_not_displayed);
            redirect(MainActivity.class);
        }
    }
}
