package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.Recipe;

public interface OnRecipeImageClickListener
{
    void onRecipeImageClick(Recipe recipe);
}
