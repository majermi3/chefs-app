package com.majernik.chefsapp.activity.adapter.listener;

import com.majernik.chefsapp.model.RecipeIngredient;

public interface OnIngredientDeleteClickListener
{
    void onIngredientDeleteClick(RecipeIngredient ingredient);
}
