package com.majernik.chefsapp.activity.adapter.listener;

import androidx.recyclerview.widget.RecyclerView;

public interface AdapterSwipeLister
{
    void onSwipe(RecyclerView.ViewHolder viewHolder, int direction, int position);
}
