package com.majernik.chefsapp.activity.fragment.friends;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.UserListAdapter;
import com.majernik.chefsapp.activity.adapter.listener.FriendshipListener;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.model.User;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

public class FriendsFragment extends BaseFriendsFragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_friends, container, false);

        userList = view.findViewById(R.id.users_list);

        if (user != null && userListAdapter == null) {
            initUserList(user.getFriends());
        }

        return view;
    }

    @Override
    protected void userLoaded()
    {
        if (userListAdapter == null && view != null) {
            initUserList(user.getFriends());
        }
    }

    private void initUserList(List<User> users)
    {
        userListAdapter = new UserListAdapter(users, new FriendshipListener()
        {
            @Override
            public void removeFriendship(final User friend)
            {
                userService.removeFriendship(friend.getId(),
                        new ApiServiceInterface.OnFinishedListener()
                        {
                            @Override
                            public void onFinished(ApiResponse response)
                            {
                                showToast("Friendship has been removed.");
                                user.removeFriend(friend);
                                userListAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onFailure(Throwable t)
                            {
                                showToast("Frendship could not be removed");
                                System.out.println(t.getMessage());
                            }
                        });
            }
        });
        userList.setAdapter(userListAdapter);
        userList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    /**
     * @return A new instance of fragment FriendsFragment.
     */
    public static FriendsFragment newInstance()
    {
        return new FriendsFragment();
    }
}
