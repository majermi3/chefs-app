package com.majernik.chefsapp.activity;

import android.os.Bundle;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.ShoppingListAdapter;
import com.majernik.chefsapp.activity.adapter.callback.ItemSwipeHelperCallback;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeIngredientSwipedListener;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.service.CartService;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ShoppingCartActivity extends BaseActivity
{
    private CartService cartService;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_shopping_cart);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void userLoaded()
    {
        cartService = new CartService(getApplicationContext());
        RecyclerView ingredientsList = findViewById(R.id.ingredients_list);

        ShoppingListAdapter ingredientListAdapter = new ShoppingListAdapter(
                cartService.getRecipeIngredients(), new OnRecipeIngredientSwipedListener()
        {
            @Override
            public void recipeIngredientSwiped(RecipeIngredient recipeIngredient)
            {
                cartService.removeRecipeIngredientFromCart(recipeIngredient);
                showToast(R.string.toast__cart__item_bought);
            }
        });
        ingredientsList.setAdapter(ingredientListAdapter);
        ingredientsList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        ItemTouchHelper.Callback callback = new ItemSwipeHelperCallback(0, ItemTouchHelper.LEFT,
                ingredientListAdapter);
        ItemTouchHelper swipeHelper = new ItemTouchHelper(callback);
        swipeHelper.attachToRecyclerView(ingredientsList);
    }
}
