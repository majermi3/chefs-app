package com.majernik.chefsapp.activity.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.listener.PendingFriendshipListener;
import com.majernik.chefsapp.activity.adapter.listener.NewFriendshipListener;
import com.majernik.chefsapp.activity.adapter.listener.FriendshipListener;
import com.majernik.chefsapp.api.UrlUtility;
import com.majernik.chefsapp.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder>
{
    private List<User> users;
    private NewFriendshipListener newFriendshipListener;
    private PendingFriendshipListener pendingFriendshipListener;
    private FriendshipListener friendshipListener;

    public UserListAdapter(List<User> users, NewFriendshipListener newFriendshipListener)
    {
        this.users = users;
        this.newFriendshipListener = newFriendshipListener;
    }

    public UserListAdapter(List<User> users, FriendshipListener friendshipListener)
    {
        this.users = users;
        this.friendshipListener = friendshipListener;
    }

    public UserListAdapter(List<User> users, PendingFriendshipListener pendingFriendshipListener)
    {
        this.users = users;
        this.pendingFriendshipListener = pendingFriendshipListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View usersListView = inflater.inflate(R.layout.list_users, parent, false);

        return new UserListAdapter.ViewHolder(usersListView, parent.getContext(), getViewType());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        User user = users.get(position);
        holder.setName(user.getName());

        if (user.hasImage()) {
            holder.setImage(user.getImage());
        }

        if (newFriendshipListener != null) {
            holder.bind(user, newFriendshipListener);
        } else if (pendingFriendshipListener != null) {
            holder.bind(user, pendingFriendshipListener);
        } else if (friendshipListener != null) {
            holder.bind(user, friendshipListener);
        }
    }

    @Override
    public int getItemCount()
    {
        return users.size();
    }

    public void setUsers(List<User> users)
    {
        this.users = users;
        this.notifyDataSetChanged();
    }

    public void addUser(User user)
    {
        this.users.add(user);
        this.notifyItemInserted(this.getItemCount() - 1);
    }

    public void removeUser(User user)
    {
        int index = this.users.indexOf(user);
        this.notifyItemRemoved(index);
        this.users.remove(user);
    }

    private int getViewType()
    {
        if (newFriendshipListener != null) {
            return ViewHolder.VIEW_NEW_FRIENDS;
        } else if (pendingFriendshipListener != null) {
            return ViewHolder.VIEW_PENDING_REQUESTS;
        } else if (friendshipListener != null) {
            return ViewHolder.VIEW_FRIENDS;
        }
        return -1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public static final int VIEW_NEW_FRIENDS = 0;
        public static final int VIEW_PENDING_REQUESTS = 1;
        public static final int VIEW_FRIENDS = 2;

        private TextView nameView;
        private CircleImageView imageView;
        private Button addFriendButton, removeFriendshipButton, rejectFriendshipButton,
                acceptFriendshipButton;
        private int viewType;

        private final Animation scaleDown, scaleUp;

        public ViewHolder(View itemView, Context context, int viewType)
        {
            super(itemView);

            this.viewType = viewType;

            itemView.setOnClickListener(this);

            nameView = itemView.findViewById(R.id.user_name);
            imageView = itemView.findViewById(R.id.user_picture);
            addFriendButton = itemView.findViewById(R.id.button_add_friend);
            removeFriendshipButton = itemView.findViewById(R.id.button_remove_friendship);
            rejectFriendshipButton = itemView.findViewById(R.id.button_reject_friendship);
            acceptFriendshipButton = itemView.findViewById(R.id.button_accept_friendship);

            scaleDown = AnimationUtils.loadAnimation(context, R.anim.scale_down);
            scaleUp = AnimationUtils.loadAnimation(context, R.anim.scale_up);
        }

        public void bind(final User user, final PendingFriendshipListener listener)
        {
            acceptFriendshipButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    listener.acceptPendingFriendship(user);
                }
            });
            rejectFriendshipButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    listener.rejectPendingFriendship(user);
                }
            });
        }

        public void bind(final User user, final FriendshipListener listener)
        {
            removeFriendshipButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    listener.removeFriendship(user);
                }
            });
        }

        public void bind(final User user, final NewFriendshipListener listener)
        {
            addFriendButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    listener.onFriendshipRequest(user);
                }
            });
        }

        public void setName(String name)
        {
            nameView.setText(name);
        }

        public void setImage(String image)
        {
            Picasso.get().load(UrlUtility.getUrl(image)).into(imageView);
        }

        @Override
        public void onClick(View view)
        {
            if (viewType == VIEW_NEW_FRIENDS) {
                toggleButton(addFriendButton);
            } else if (viewType == VIEW_FRIENDS) {
                toggleButton(removeFriendshipButton);
            } else if (viewType == VIEW_PENDING_REQUESTS) {
                toggleButton(acceptFriendshipButton);
                toggleButton(rejectFriendshipButton);
            }
        }

        private void toggleButton(Button button)
        {
            if (button.getVisibility() == View.VISIBLE) {
                hideButton(button);
            } else {
                showButton(button);
            }
        }

        private void showButton(Button button)
        {
            button.setVisibility(View.VISIBLE);
            button.startAnimation(scaleDown);
        }

        private void hideButton(Button button)
        {
            button.startAnimation(scaleUp);
            button.setVisibility(View.GONE);
        }
    }
}
