package com.majernik.chefsapp.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.contract.SignUpContract;
import com.majernik.chefsapp.model.User;
import com.majernik.chefsapp.presenter.SignUpPresenter;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

public class SignUpActivity extends BaseActivity implements SignUpContract.View, View.OnClickListener, Validator.ValidationListener
{
    private SignUpPresenter suPresenter;

    private Validator validator;

    @NotEmpty
    @Email
    private EditText email;

    @NotEmpty
    private EditText name;

    @NotEmpty
    @Password(min = 8, scheme = Password.Scheme.ALPHA_NUMERIC)
    private EditText password;

    @ConfirmPassword
    private EditText passwordAgain;

    private Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        suPresenter = new SignUpPresenter(this);

        name = this.findViewById(R.id.input_name);
        email = this.findViewById(R.id.input_email);
        password = this.findViewById(R.id.input_password);
        passwordAgain = this.findViewById(R.id.input_password_again);

        signUp = this.findViewById(R.id.button_sign_up);
        signUp.setOnClickListener(this);

        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    protected void userLoaded()
    {

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.button_sign_up:
                validator.validate();
                break;
        }
    }

    @Override
    public void showLogin()
    {
        Toast toast = Toast.makeText(getApplicationContext(), "Registration successful",
                Toast.LENGTH_SHORT);
        toast.show();
        redirect(LoginActivity.class);
    }

    @Override
    public void showError(String message)
    {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onValidationSucceeded()
    {
        User user = new User();
        user.setName(name.getText().toString().trim());
        user.setEmail(email.getText().toString().trim());
        user.setPassword(password.getText().toString().trim());

        suPresenter.signUp(user);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors)
    {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
