package com.majernik.chefsapp.activity.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.listener.OnIngredientDeleteClickListener;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.service.SettingsService;

import java.util.List;

public class IngredientListAdapter extends RecyclerView.Adapter<IngredientListAdapter.ViewHolder>
{

    private List<RecipeIngredient> ingredients;
    private OnIngredientDeleteClickListener onIngredientDeleteClickListener;

    public IngredientListAdapter(List<RecipeIngredient> ingredients)
    {
        this.ingredients = ingredients;
    }

    public IngredientListAdapter(List<RecipeIngredient> ingredients,
                                 OnIngredientDeleteClickListener onIngredientDeleteClickListener)
    {
        this.ingredients = ingredients;
        this.onIngredientDeleteClickListener = onIngredientDeleteClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View ingredientListView = inflater.inflate(R.layout.list_ingredients, parent, false);

        return new ViewHolder(ingredientListView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        RecipeIngredient recipeIngredient = ingredients.get(position);
        Context context = holder.itemView.getContext();
        SettingsService settingsService = new SettingsService(context);

        holder.setItemText(
                recipeIngredient.toString(context.getResources(), settingsService.loadSettings())
        );
        if (onIngredientDeleteClickListener != null) {
            holder.bindOnImageClick(recipeIngredient, onIngredientDeleteClickListener);
        } else {
            holder.hideDeleteButton();
        }
    }

    /**
     * Sets ingredients
     *
     * @param ingredients
     */
    public void setIngredients(List<RecipeIngredient> ingredients)
    {
        this.ingredients = ingredients;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        return ingredients.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView item;
        ImageButton deleteButton;

        public ViewHolder(View itemView)
        {
            super(itemView);

            item = itemView.findViewById(R.id.ingredient_list_item);
            deleteButton = itemView.findViewById(R.id.button_delete_ingredient);
        }

        /**
         * Sets text to item
         *
         * @param text
         */
        public void setItemText(String text)
        {
            item.setText(text);
        }

        /**
         * Hides delete button
         */
        public void hideDeleteButton()
        {
            deleteButton.setVisibility(View.INVISIBLE);
        }

        /**
         * Binds click listener to delete button
         *
         * @param ingredient
         * @param onIngredientDeleteClickListener
         */
        public void bindOnImageClick(final RecipeIngredient ingredient,
                                     final OnIngredientDeleteClickListener onIngredientDeleteClickListener)
        {
            deleteButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    onIngredientDeleteClickListener.onIngredientDeleteClick(ingredient);
                }
            });
        }
    }
}
