package com.majernik.chefsapp.activity.adapter;

import android.animation.Animator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.adapter.listener.AdapterSwipeLister;
import com.majernik.chefsapp.activity.adapter.listener.OnRecipeIngredientSwipedListener;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.service.SettingsService;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> implements AdapterSwipeLister
{
    private ArrayList<RecipeIngredient> recipeIngredients;
    private OnRecipeIngredientSwipedListener onRecipeIngredientSwipedListener;

    public ShoppingListAdapter(
            ArrayList<RecipeIngredient> recipeIngredients,
            OnRecipeIngredientSwipedListener onRecipeIngredientSwipedListener
    )
    {
        this.recipeIngredients = recipeIngredients;
        this.onRecipeIngredientSwipedListener = onRecipeIngredientSwipedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View ingredientListView = inflater.inflate(R.layout.list_cart_items, parent, false);

        return new ShoppingListAdapter.ViewHolder(ingredientListView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        RecipeIngredient recipeIngredient = recipeIngredients.get(position);
        Context context = holder.itemView.getContext();
        SettingsService settingsService = new SettingsService(context);

        holder.setItemText(
                recipeIngredient.toString(context.getResources(), settingsService.loadSettings())
        );
    }

    @Override
    public int getItemCount()
    {
        return recipeIngredients.size();
    }

    @Override
    public void onSwipe(RecyclerView.ViewHolder viewHolder, int direction, int position)
    {
        final int realPosition = viewHolder.getAdapterPosition();
        if (realPosition < recipeIngredients.size()) {
            final RecipeIngredient recipeIngredient = recipeIngredients.get(realPosition);
            recipeIngredients.remove(recipeIngredient);

            viewHolder.itemView
                    .animate()
                    .setDuration(1000)
                    .setListener(new Animator.AnimatorListener()
                    {
                        @Override
                        public void onAnimationStart(Animator animation)
                        {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation)
                        {
                            notifyDataSetChanged();
                            onRecipeIngredientSwipedListener.recipeIngredientSwiped(
                                    recipeIngredient);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation)
                        {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation)
                        {

                        }
                    }).start();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView item;
        public View foregroundView, backgroundView;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            item = itemView.findViewById(R.id.ingredient_list_item);
            foregroundView = itemView.findViewById(R.id.view_foreground);
            backgroundView = itemView.findViewById(R.id.view_background);
        }

        public void setItemText(String text)
        {
            item.setText(text);
        }
    }
}
