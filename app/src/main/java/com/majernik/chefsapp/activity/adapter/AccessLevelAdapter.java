package com.majernik.chefsapp.activity.adapter;

import android.content.Context;

import androidx.annotation.NonNull;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.majernik.chefsapp.R;
import com.majernik.chefsapp.model.AccessLevel;

public class AccessLevelAdapter<A> extends ArrayAdapter
{

    private AccessLevel[] values;

    public AccessLevelAdapter(Context context, int resource, @NonNull AccessLevel[] values)
    {
        super(context, resource, values);
        this.values = values;
    }

    public int getPosition(int accessLevelValue)
    {
        for (int i = 0; i < values.length; i++) {
            if (values[i].getValue() == accessLevelValue) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view = getCustomView(parent);
        setEnumValueText(position, view);
        return view;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        View view = getCustomView(parent);
        setEnumValueText(position, view);
        return view;
    }

    private View getCustomView(ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.enumspinner_listitem, parent, false);
        return view;
    }

    private void setEnumValueText(int position, View view)
    {
        AccessLevel value = (AccessLevel) getItem(position);
        TextView lblTextView = (TextView) view.findViewById(R.id.lblTextView);
        lblTextView.setText(getContext().getString(value.getTitleId()));
    }
}

