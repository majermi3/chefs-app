package com.majernik.chefsapp.activity.adapter.callback;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.majernik.chefsapp.activity.adapter.listener.AdapterMoveListener;

public class ItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private AdapterMoveListener listener;
    private int dragFrom = -1;
    private int dragTo = -1;

    public ItemTouchHelperCallback(AdapterMoveListener listener)
    {
        this.listener = listener;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder)
    {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;

        return makeMovementFlags(dragFlags, 0);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target)
    {
        int fromPosition = viewHolder.getAdapterPosition();
        int toPosition = target.getAdapterPosition();

        if(dragFrom == -1) {
            dragFrom = fromPosition;
        }
        dragTo = toPosition;

        listener.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
    {

    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder)
    {
        super.clearView(recyclerView, viewHolder);

        if(dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {
            listener.onItemDrop(dragFrom, dragTo);
        }

        dragFrom = dragTo = -1;
    }
}
