package com.majernik.chefsapp.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.TokenResponse;
import com.majernik.chefsapp.contract.MainContract;
import com.majernik.chefsapp.model.User;
import com.majernik.chefsapp.presenter.MainPresenter;
import com.majernik.chefsapp.R;
import com.majernik.chefsapp.service.TokenService;
import com.majernik.chefsapp.utility.Session;

public class MainActivity extends BaseActivity implements MainContract.View
{
    public static final int RC_SIGN_IN = 30;

    private MainPresenter mPresenter;
    private TokenService tokenService;
    private GoogleSignInClient mGoogleSignInClient;

    private Button loginButton, signUpButton;
    private SignInButton googleLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPresenter = new MainPresenter(this);
        tokenService = new TokenService(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(
                GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        loginButton = findViewById(R.id.button_login_chef);
        loginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showLogin();
            }
        });

        googleLoginButton = findViewById(R.id.google_sign_in_button);
        googleLoginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showGoogleLogin();
            }
        });

        signUpButton = findViewById(R.id.button_register);
        signUpButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showSignUp();
            }
        });

        loginUser();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = "fcm_default_channel";
            String channelName = "My channel";
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }
    }

    @Override
    protected void userLoaded()
    {
        System.out.println("user loaded");
    }

    @Override
    protected void userNotLoaded(Throwable t)
    {
        System.out.println("user not loaded");
        System.out.println(t.getMessage());
    }

    private void createMainActivity()
    {
    }

    private void loginUser()
    {
        String refreshToken = getSharedPreferences("USER", Context.MODE_PRIVATE).getString(
                "refreshToken", null);
        GoogleSignInAccount googleUser = GoogleSignIn.getLastSignedInAccount(this);

        if (googleUser != null) {
            Session.setToken(googleUser.getIdToken());
            showDashboard();
        } else if (refreshToken != null) {
            tokenService.requestAsyncToken(new ApiServiceInterface.TokenService.OnFinishedListener()
            {
                @Override
                public void onFinished(TokenResponse tokenResponse)
                {
                    if (tokenResponse.isSuccessful()) {
                        Session.setToken(tokenResponse.getToken());
                        tokenService.saveRefreshToken(tokenResponse.getRefreshToken());
                        showDashboard();
                    }
                }

                @Override
                public void onFailure(Throwable t)
                {
                    createMainActivity();
                }
            });
        }
    }

    private void silentLogin()
    {
        Task<GoogleSignInAccount> task = mGoogleSignInClient.silentSignIn();
        handleSignInResult(task);
    }

    private void showGoogleLogin()
    {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void showLogin()
    {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void showSignUp()
    {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask)
    {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            User user = getUserFromGoogleAccount(account);

            oauth(user, account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("GOOGLE LOGIN ERROR", "signInResult:failed code=" + e.getStatusCode());
            showToast(R.string.toast__login__cannot_login);
        }
    }

    private User getUserFromGoogleAccount(GoogleSignInAccount account)
    {
        User user = new User();
        user.setName(account.getDisplayName());
        user.setEmail(account.getEmail());
        if (account.getPhotoUrl() != null) {
            user.setImage(account.getPhotoUrl().toString());
        }
        user.setProvider(User.PROVIDER_GOOGLE);
        user.setProviderId(account.getId());

        return user;
    }

    private void oauth(final User user, final GoogleSignInAccount account)
    {
        tokenService.oauth(account.getIdToken(),
                new ApiServiceInterface.TokenService.OnFinishedListener()
                {
                    @Override
                    public void onFinished(TokenResponse tokenResponse)
                    {
                        if (tokenResponse.getStatus() == 401) {
                            userService.createUser(user,
                                    new ApiServiceInterface.OnFinishedListener()
                                    {
                                        @Override
                                        public void onFinished(ApiResponse response)
                                        {
                                            if (response.isSuccessful()) {
                                                Session.setToken(account.getIdToken());
                                                redirectAndFinish(DashboardActivity.class);
                                            } else {
                                                showToast(R.string.toast__login__cannot_login);
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t)
                                        {
                                            showToast(R.string.toast__login__cannot_login);
                                        }
                                    });
                        } else {
                            Session.setToken(account.getIdToken());
                        }
                    }

                    @Override
                    public void onFailure(Throwable t)
                    {

                    }
                });
    }

    public void showDashboard()
    {
        redirectAndFinish(DashboardActivity.class);
    }
}
