package com.majernik.chefsapp.api;

import android.content.Context;

import com.majernik.chefsapp.api.interceptor.RefreshTokenInterceptor;
import com.majernik.chefsapp.service.TokenService;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SecureApiClient
{
    private static Retrofit retrofit = null;

    /**
     * This method returns retrofit client instance
     *
     * @return Retrofit object
     */
    public static Retrofit getClient(Context context)
    {
        if (retrofit == null) {
            TokenService tokenService = new TokenService(context);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.interceptors().add(new RefreshTokenInterceptor(tokenService));

            int cacheSize = 10 * 1024 * 1024; // 10 MB
            Cache cache = new Cache(context.getCacheDir(), cacheSize);

            OkHttpClient client = httpClient
                    .cache(cache)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(ApiClient.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}
