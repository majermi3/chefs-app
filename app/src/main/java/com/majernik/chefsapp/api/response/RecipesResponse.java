package com.majernik.chefsapp.api.response;

import com.majernik.chefsapp.model.Recipe;

import java.util.List;

public class RecipesResponse extends ApiResponse
{
    private List<Recipe> recipes;

    public List<Recipe> getRecipes()
    {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes)
    {
        this.recipes = recipes;
    }
}
