package com.majernik.chefsapp.api.response;

import com.majernik.chefsapp.model.User;

public class UserResponse extends ApiResponse
{
    protected User user;

    public UserResponse(User user)
    {
        this.user = user;
        this.status = 200;
    }

    public UserResponse(int status, String message, String id, User user)
    {
        super(status, message, id);
        this.user = user;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    @Override
    public boolean isSuccessful()
    {
        return super.isSuccessful() && this.getUser() != null;
    }
}
