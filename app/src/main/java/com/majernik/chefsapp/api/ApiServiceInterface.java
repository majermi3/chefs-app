package com.majernik.chefsapp.api;

import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.IngredientsResponse;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.api.response.RecipesResponse;
import com.majernik.chefsapp.api.response.TokenResponse;
import com.majernik.chefsapp.api.response.UserResponse;
import com.majernik.chefsapp.api.response.UsersResponse;
import com.majernik.chefsapp.model.Ingredient;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.User;

import java.util.TreeMap;

import retrofit2.Response;

public interface ApiServiceInterface
{
    interface UserService
    {
        interface OnFinishedListener
        {
            void onFinished(UserResponse userResponse);

            void onFailure(Throwable t);
        }

        interface OnUsersFinishedListener
        {
            void onFinished(UsersResponse usersResponse);

            void onFailure(Throwable t);
        }

        void findUsers(String term, OnUsersFinishedListener onFinishedListener);

        void createUser(User user, ApiServiceInterface.OnFinishedListener onFinishedListener);

        void updateCurrentUser(User user,
                               ApiServiceInterface.OnFinishedListener onFinishedListener);

        void loginUser(String email, String password,
                       TokenService.OnFinishedListener onFinishedListener);

        void markRecipeAsFavorite(Recipe recipe,
                                  ApiServiceInterface.OnFinishedListener onFinishedListener);

        void unmarkRecipeAsFavorite(Recipe recipe,
                                    ApiServiceInterface.OnFinishedListener onFinishedListener);
    }

    interface TokenService
    {
        interface OnFinishedListener
        {
            void onFinished(TokenResponse tokenResponse);

            void onFailure(Throwable t);
        }

        Response<TokenResponse> requestToken();

        void oauth(String token,
                   ApiServiceInterface.TokenService.OnFinishedListener onFinishedListener);
    }

    interface RecipeService
    {
        interface OnFinishedListener
        {
            void onFinished(RecipesResponse recipesResponse);

            void onFailure(Throwable t);
        }

        void getRecipes(OnFinishedListener onFinishedListener);

        void getMyRecipes(OnFinishedListener onFinishedListener);
    }

    interface IngredientService
    {
        interface OnFinishedListener
        {
            void onFinished(IngredientsResponse ingredientsResponse);

            void onFailure(Throwable t);
        }

        interface OnSearchFinishedListener
        {
            void onFinished(TreeMap<Integer, Ingredient> distanceMap);

            void onFailure(Throwable t);
        }

        void findIngredient(CharSequence token, OnSearchFinishedListener onSearchFinishedListener);

        void getIngredients(OnFinishedListener onFinishedListener);
    }

    interface RecipeIngredientService
    {

    }

    interface OnMediaUploadedListener
    {
        void onFinished(MediaResponse response);

        void onFailure(Throwable t);
    }

    interface OnFinishedListener
    {
        void onFinished(ApiResponse response);

        void onFailure(Throwable t);
    }
}
