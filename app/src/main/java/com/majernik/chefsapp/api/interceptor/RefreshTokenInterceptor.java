package com.majernik.chefsapp.api.interceptor;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.majernik.chefsapp.api.response.TokenResponse;
import com.majernik.chefsapp.service.TokenService;
import com.majernik.chefsapp.utility.Session;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class RefreshTokenInterceptor implements Interceptor
{
    private TokenService tokenService;

    public RefreshTokenInterceptor(TokenService tokenService)
    {
        this.tokenService = tokenService;
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();

        Response response = chain.proceed(request);

        // If the request is not successful get new token and rebuild the request
        if (response.code() == 401 || response.code() == 403) {
            retrofit2.Response<TokenResponse> tokenResponse = tokenService.requestToken();

            String token = tokenResponse.body().getToken();
            String refreshToken = tokenResponse.body().getRefreshToken();

            // If token or refreshToken was not given, return original 401/403 response
            if (token == null || refreshToken == null) {
                GoogleSignInAccount account = tokenService.oauthSilentSignIn();
                if (account == null || account.getIdToken().isEmpty()) {
                    return response;
                }
                token = account.getIdToken();
            }

            Session.setToken(token);
            tokenService.saveRefreshToken(refreshToken);

            Request.Builder builder = response
                    .request()
                    .newBuilder()
                    .header("Authorization", token);

            return chain.proceed(builder.build());
        }

        return response;
    }
}
