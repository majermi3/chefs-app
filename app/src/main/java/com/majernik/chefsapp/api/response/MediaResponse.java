package com.majernik.chefsapp.api.response;

public class MediaResponse extends ApiResponse
{
    protected String path;

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public boolean hasPath()
    {
        return path != null && !path.isEmpty();
    }

    @Override
    public boolean isSuccessful()
    {
        return super.isSuccessful() && hasPath();
    }
}
