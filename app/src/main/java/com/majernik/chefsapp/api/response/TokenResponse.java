package com.majernik.chefsapp.api.response;

public class TokenResponse extends ApiResponse
{
    protected String token;
    protected String refreshToken;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken)
    {
        this.refreshToken = refreshToken;
    }

    @Override
    public boolean isSuccessful()
    {
        return super.isSuccessful() && this.getToken() != null && this.getToken() != "";
    }
}
