package com.majernik.chefsapp.api.response;

import com.majernik.chefsapp.model.User;

import java.util.List;

public class UsersResponse extends ApiResponse
{
    private List<User> users;

    public UsersResponse(List<User> users)
    {
        this.users = users;
        this.status = 200;
    }

    public UsersResponse(int status, String message, String id, List<User> users)
    {
        super(status, message, id);
        this.users = users;
    }

    public List<User> getUsers()
    {
        return users;
    }

    public void setUsers(List<User> users)
    {
        this.users = users;
    }

    @Override
    public boolean isSuccessful()
    {
        return super.isSuccessful() && this.getUsers() != null;
    }
}
