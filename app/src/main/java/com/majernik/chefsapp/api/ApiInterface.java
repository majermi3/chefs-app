package com.majernik.chefsapp.api;

import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.IngredientsResponse;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.api.response.RecipesResponse;
import com.majernik.chefsapp.api.response.TokenResponse;
import com.majernik.chefsapp.api.response.UserResponse;
import com.majernik.chefsapp.api.response.UsersResponse;
import com.majernik.chefsapp.model.Ingredient;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.model.RecipeStep;
import com.majernik.chefsapp.model.User;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface
{
    @POST("users")
    Call<ApiResponse> createUser(@Body User user);

    @GET("users")
    Call<UsersResponse> findUsers(@Header("Authorization") String token,
                                  @Query(value = "term", encoded = true) String term);

    @GET("users/me")
    Call<UserResponse> getCurrentUser(@Header("Authorization") String token);

    @POST("users/me")
    Call<ApiResponse> updateCurrentUser(@Header("Authorization") String token, @Body User user);

    @Multipart
    @POST("users/me/image")
    Call<MediaResponse> uploadProfileImage(@Header("Authorization") String token,
                                           @Part MultipartBody.Part image);

    @POST("users/me/reject-friendship-request/{friendId}")
    Call<ApiResponse> rejectFriendshipRequest(@Header("Authorization") String token,
                                              @Path("friendId") String friendId);

    @POST("users/me/accept-friendship-request/{friendId}")
    Call<ApiResponse> acceptFriendshipRequest(@Header("Authorization") String token,
                                              @Path("friendId") String friendId);

    @POST("users/me/send-friendship-request/{friendId}")
    Call<ApiResponse> sendFriendshipRequest(@Header("Authorization") String token,
                                            @Path("friendId") String friendId);

    @DELETE("users/me/friendship/{friendId}")
    Call<ApiResponse> removeFriendship(@Header("Authorization") String token,
                                       @Path("friendId") String friendId);

    @POST("users/me/favorite-recipes/{recipeId}")
    Call<ApiResponse> markRecipeAsFavorite(@Header("Authorization") String token,
                                           @Path("recipeId") String recipeId);

    @DELETE("users/me/favorite-recipes/{recipeId}")
    Call<ApiResponse> unmarkRecipeAsFavorite(@Header("Authorization") String token,
                                             @Path("recipeId") String recipeId);

    @FormUrlEncoded
    @POST("auth")
    Call<TokenResponse> loginUser(@Field("email") String email,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("auth")
    Call<TokenResponse> loginUser(@Field("email") String email,
                                  @Field("token") String token,
                                  @Field("provider") String provider);

    @FormUrlEncoded
    @POST("auth/token")
    Call<TokenResponse> getToken(@Field("refreshToken") String refreshToken);

    @FormUrlEncoded
    @POST("auth/oauth")
    Call<TokenResponse> oauth(@Field("token") String token);

    @GET("recipes/")
    Call<RecipesResponse> getRecipes(@Header("Authorization") String token);

    @GET("recipes/my")
    Call<RecipesResponse> getMyRecipes(@Header("Authorization") String token);

    @POST("recipes")
    Call<ApiResponse> createRecipe(@Header("Authorization") String token,
                                   @Body Recipe recipe);

    @PUT("recipes")
    Call<ApiResponse> updateRecipe(@Header("Authorization") String token,
                                   @Body Recipe recipe);

    @DELETE("recipes/{recipeId}")
    Call<ApiResponse> deleteRecipe(@Header("Authorization") String token,
                                   @Path("recipeId") String recipeId);

    @GET("ingredients")
    Call<IngredientsResponse> getIngredients(@Header("Authorization") String token);

    @POST("ingredients")
    Call<ApiResponse> createIngredient(@Header("Authorization") String token,
                                       @Body Ingredient ingredient);

    @POST("recipes/{recipeId}/ingredients")
    Call<ApiResponse> createRecipeIngredient(@Header("Authorization") String token,
                                             @Path("recipeId") String recipeId,
                                             @Body RecipeIngredient recipeIngredient);

    @PUT("recipes/{recipeId}/ingredients/{recipeIngredientId}")
    Call<ApiResponse> updateRecipeIngredient(@Header("Authorization") String token,
                                             @Path("recipeId") String recipeId,
                                             @Path("recipeIngredientId") String recipeIngredientId,
                                             @Body RecipeIngredient recipeIngredient);

    @DELETE("recipes/{recipeId}/ingredients/{recipeIngredientId}")
    Call<ApiResponse> removeRecipeIngredient(@Header("Authorization") String token,
                                             @Path("recipeId") String recipeId,
                                             @Path("recipeIngredientId") String recipeIngredientId);

    @POST("recipes/{recipeId}/steps")
    Call<ApiResponse> createRecipeStep(@Header("Authorization") String token,
                                       @Path("recipeId") String recipeId,
                                       @Body RecipeStep recipeStep);

    @PUT("recipes/{recipeId}/steps/{recipeStepId}")
    Call<ApiResponse> updateRecipeStep(@Header("Authorization") String token,
                                       @Path("recipeId") String recipeId,
                                       @Path("recipeStepId") String recipeStepId,
                                       @Body RecipeStep recipeStep);

    @Multipart
    @POST("recipes/{recipeId}/steps/{recipeStepId}/media")
    Call<MediaResponse> uploadRecipeStepMedia(@Header("Authorization") String token,
                                              @Path("recipeId") String recipeId,
                                              @Path("recipeStepId") String recipeStepId,
                                              @Part MultipartBody.Part media);

    @Multipart
    @POST("recipes/{recipeId}/steps/{recipeStepId}/thumbnail")
    Call<MediaResponse> uploadRecipeStepThumbnail(@Header("Authorization") String token,
                                                  @Path("recipeId") String recipeId,
                                                  @Path("recipeStepId") String recipeStepId,
                                                  @Part MultipartBody.Part image);

    @DELETE("recipes/{recipeId}/steps/{recipeStepId}")
    Call<ApiResponse> removeRecipeStep(@Header("Authorization") String token,
                                       @Path("recipeId") String recipeId,
                                       @Path("recipeStepId") String recipeStepId);

    @Multipart
    @POST("recipes/{recipeId}/image")
    Call<MediaResponse> uploadRecipeImage(@Header("Authorization") String token,
                                          @Path("recipeId") String recipeId,
                                          @Part MultipartBody.Part image);

}
