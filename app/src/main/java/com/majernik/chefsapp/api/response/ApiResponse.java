package com.majernik.chefsapp.api.response;

import java.io.Serializable;

public class ApiResponse implements Serializable
{
    protected int status;
    protected String message;
    protected String id;

    public ApiResponse()
    {
    }

    public ApiResponse(int status, String message)
    {
        this.status = status;
        this.message = message;
    }

    public ApiResponse(int status, String message, String id)
    {
        this.status = status;
        this.message = message;
        this.id = id;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public boolean isSuccessful()
    {
        return this.getStatus() == 200;
    }
}
