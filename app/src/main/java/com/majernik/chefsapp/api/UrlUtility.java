package com.majernik.chefsapp.api;

public class UrlUtility
{
    public static String getUrl(String relativePath)
    {
        return ApiClient.BASE_URL + relativePath.replaceAll("^/+", "");
    }
}
