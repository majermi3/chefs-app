package com.majernik.chefsapp.api.response;

import com.majernik.chefsapp.model.Ingredient;

import java.util.List;

public class IngredientsResponse extends ApiResponse
{
    private List<Ingredient> ingredients;

    public List<Ingredient> getIngredients()
    {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients)
    {
        this.ingredients = ingredients;
    }

    public void addIngredient(Ingredient ingredient)
    {
        ingredients.add(ingredient);
    }

    public void removeIngredient(Ingredient ingredient)
    {
        ingredients.remove(ingredient);
    }
}
