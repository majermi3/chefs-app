package com.majernik.chefsapp.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.majernik.chefsapp.model.Settings;

public class SettingsService
{
    private Context context;
    private SharedPreferences sharedPref;

    public SettingsService(Context context)
    {
        this.context = context;
        this.sharedPref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
    }

    public void saveSettings(Settings settings)
    {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("settings", serialize(settings));
        editor.apply();
    }

    public Settings loadSettings()
    {
        String json = sharedPref.getString("settings", null);
        Settings settings = unSerialize(json);
        if (settings == null) {
            settings = new Settings(Settings.UNITS_METRIC);
            saveSettings(settings);
        }
        return settings;
    }

    private String serialize(Settings settings)
    {
        Gson gson = new Gson();
        return gson.toJson(settings);
    }

    private Settings unSerialize(String json)
    {
        Gson gson = new Gson();
        if (json != null) {
            return gson.fromJson(json, Settings.class);
        }
        return null;
    }
}
