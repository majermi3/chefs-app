package com.majernik.chefsapp.service;

import android.content.Context;
import android.graphics.Bitmap;

import com.majernik.chefsapp.api.ApiInterface;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeStep;
import com.majernik.chefsapp.utility.Session;

import java.io.File;
import java.io.IOException;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeStepsService extends BaseService
{
    public RecipeStepsService(Context context)
    {
        super(context);
    }

    public void saveRecipeStep(Recipe recipe, RecipeStep recipeStep,
                               ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        if (recipeStep.getId() == null) {
            createRecipeStep(recipe, recipeStep, onFinishedListener);
        } else {
            updateRecipeStep(recipe, recipeStep, onFinishedListener);
        }
    }

    public void createRecipeStep(Recipe recipe, RecipeStep recipeStep,
                                 ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.createRecipeStep(Session.getToken(), recipe.getId(),
                recipeStep);

        enqueue(call, onFinishedListener);
    }

    public void updateRecipeStep(Recipe recipe, RecipeStep recipeStep,
                                 ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.updateRecipeStep(Session.getToken(), recipe.getId(),
                recipeStep.getId(), recipeStep);

        enqueue(call, onFinishedListener);
    }

    public void removeRecipeStep(Recipe recipe, RecipeStep recipeStep,
                                 ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.removeRecipeStep(Session.getToken(), recipe.getId(),
                recipeStep.getId());

        enqueue(call, onFinishedListener);
    }

    public void uploadRecipeStepImage(Recipe recipe, RecipeStep step, Bitmap image,
                                      final ApiServiceInterface.OnMediaUploadedListener onFinishedListener) throws IOException
    {
        MultipartBody.Part body = getImageMultipartBody(image);
        uploadRecipeStepMedia(recipe, step, body, onFinishedListener);
    }

    public void uploadRecipeStepVideo(Recipe recipe, RecipeStep step, File video,
                                      final ApiServiceInterface.OnMediaUploadedListener onFinishedListener) throws IOException
    {
        MultipartBody.Part body = getVideoMultipartBody(video);
        uploadRecipeStepMedia(recipe, step, body, onFinishedListener);
    }

    public void uploadRecipeStepMedia(Recipe recipe, RecipeStep step, MultipartBody.Part body,
                                      final ApiServiceInterface.OnMediaUploadedListener onFinishedListener) throws IOException
    {
        ApiInterface apiService = getSecureClient();
        Call<MediaResponse> call = apiService.uploadRecipeStepMedia(
                Session.getToken(),
                recipe.getId(),
                step.getId(),
                body
        );

        enqueue(call, onFinishedListener);
    }

    public void uploadRecipeStepThumbnail(Recipe recipe, RecipeStep step, Bitmap thumbnail,
                                          final ApiServiceInterface.OnMediaUploadedListener onFinishedListener) throws IOException
    {
        ApiInterface apiService = getSecureClient();
        Call<MediaResponse> call = apiService.uploadRecipeStepThumbnail(
                Session.getToken(),
                recipe.getId(),
                step.getId(),
                getImageMultipartBody(thumbnail)
        );

        enqueue(call, onFinishedListener);
    }

    private void enqueue(Call<MediaResponse> call,
                         final ApiServiceInterface.OnMediaUploadedListener onFinishedListener)
    {
        call.enqueue(new Callback<MediaResponse>()
        {
            @Override
            public void onResponse(Call<MediaResponse> call, Response<MediaResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<MediaResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    private void enqueue(Call<ApiResponse> call,
                         final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        call.enqueue(new Callback<ApiResponse>()
        {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
