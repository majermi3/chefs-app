package com.majernik.chefsapp.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.majernik.chefsapp.model.Cart;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeIngredient;

import java.util.ArrayList;

public class CartService
{
    private Context context;
    private SharedPreferences sharedPref;

    public CartService(Context context)
    {
        this.context = context;
        this.sharedPref = context.getSharedPreferences("cart", Context.MODE_PRIVATE);
    }

    public void removeRecipeIngredientFromCart(RecipeIngredient recipeIngredient)
    {
        Cart cart = loadCart();
        cart.removeRecipeIngredient(recipeIngredient);
        saveCart(cart);
    }

    public void addRecipeToCart(Recipe recipe)
    {
        Cart cart = loadCart();
        cart.addRecipe(recipe);
        saveCart(cart);
    }

    public void saveCart(Cart cart)
    {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("cart", serialize(cart));
        editor.apply();
    }

    public Cart loadCart()
    {
        String json = sharedPref.getString("cart", null);
        Cart cart = unSerialize(json);
        if (cart == null) {
            cart = new Cart();
            saveCart(cart);
        }
        return cart;
    }

    public ArrayList<RecipeIngredient> getRecipeIngredients()
    {
        return new ArrayList<>(this.loadCart().getRecipeIngredients().values());
    }

    private String serialize(Cart cart)
    {
        Gson gson = new Gson();
        return gson.toJson(cart);
    }

    private Cart unSerialize(String json)
    {
        Gson gson = new Gson();
        if (json != null) {
            return gson.fromJson(json, Cart.class);
        }
        return null;
    }
}
