package com.majernik.chefsapp.service;

import android.content.Context;

import com.majernik.chefsapp.api.ApiInterface;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.IngredientsResponse;
import com.majernik.chefsapp.model.Ingredient;
import com.majernik.chefsapp.utility.Session;
import com.majernik.chefsapp.utility.StringUtility;

import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IngredientsService extends BaseService implements ApiServiceInterface.IngredientService
{
    private IngredientsResponse ingredientsResponse;

    public IngredientsService(Context context)
    {
        super(context);
    }

    public void createIngredient(final Ingredient ingredient,
                                 final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.createIngredient(Session.getToken(), ingredient);

        call.enqueue(new Callback<ApiResponse>()
        {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response)
            {
                ingredientsResponse.addIngredient(ingredient);
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void findIngredient(final CharSequence token,
                               final ApiServiceInterface.IngredientService.OnSearchFinishedListener onSearchFinishedListener)
    {
        getIngredients(new OnFinishedListener()
        {
            @Override
            public void onFinished(IngredientsResponse ingredientsResponse)
            {
                onSearchFinishedListener.onFinished(
                        getDistanceMap(ingredientsResponse.getIngredients(), token));
            }

            @Override
            public void onFailure(Throwable t)
            {
                onSearchFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void getIngredients(
            final ApiServiceInterface.IngredientService.OnFinishedListener onFinishedListener)
    {
        if (ingredientsResponse != null) {
            onFinishedListener.onFinished(ingredientsResponse);
        } else {
            ApiInterface apiService = getSecureClient();
            Call<IngredientsResponse> call = apiService.getIngredients(Session.getToken());

            call.enqueue(new Callback<IngredientsResponse>()
            {
                @Override
                public void onResponse(Call<IngredientsResponse> call,
                                       Response<IngredientsResponse> response)
                {
                    ingredientsResponse = response.body();
                    onFinishedListener.onFinished(ingredientsResponse);
                }

                @Override
                public void onFailure(Call<IngredientsResponse> call, Throwable t)
                {
                    onFinishedListener.onFailure(t);
                }
            });
        }
    }

    private TreeMap<Integer, Ingredient> getDistanceMap(List<Ingredient> ingredients,
                                                        CharSequence token)
    {
        TreeMap<Integer, Ingredient> distanceMap = new TreeMap<>(Collections.reverseOrder());

        for (Ingredient ingredient : ingredients) {
            String title = ingredient.getTitle().toLowerCase();
            int distance = title.indexOf(token.toString().toLowerCase());
            if (distance == -1) {
                distance = StringUtility.computeLevenshteinDistance(title, token) + token.length();
            }

            if (distance < title.length()) {
                distanceMap.put(distance, ingredient);
            }
        }

        return distanceMap;
    }
}
