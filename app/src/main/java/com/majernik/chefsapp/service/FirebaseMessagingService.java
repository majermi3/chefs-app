package com.majernik.chefsapp.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.majernik.chefsapp.R;
import com.majernik.chefsapp.activity.FriendsActivity;
import com.majernik.chefsapp.activity.ManageFriendshipRequestActivity;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.UserResponse;
import com.majernik.chefsapp.model.User;

import androidx.core.app.NotificationCompat;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService
{
    public final String CHANNEL_ID = "chefs_app_notifications";
    public final String DEBUG_TAG = "NOTIFICATIONS";
    public final String NOTIFICATION_TYPE_NEW_USER = "new_user";
    public final String NOTIFICATION_TYPE_REQUEST_ACCEPTED = "request_accepted";

    @Override
    public void onNewToken(String token)
    {
        Log.d(DEBUG_TAG, "Refreshed token: " + token);
        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(final String token)
    {
        final UserService userService = new UserService(getApplicationContext());
        userService.getCurrentUser(new ApiServiceInterface.UserService.OnFinishedListener()
        {
            @Override
            public void onFinished(UserResponse userResponse)
            {
                User user = userResponse.getUser();
                user.setFcmRefreshToken(token);
                userService.updateCurrentUser(user);
            }

            @Override
            public void onFailure(Throwable t)
            {
                Log.d(DEBUG_TAG, "Could not get current user");
            }
        });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        if (remoteMessage.getData().size() > 0) {
            Log.d(DEBUG_TAG, remoteMessage.getData().get("user"));
            Log.d(DEBUG_TAG, remoteMessage.getData().get("type"));
            sendNotification(
                    remoteMessage.getData().get("message"),
                    remoteMessage.getData().get("type"),
                    remoteMessage.getData().get("user")
            );
        } else if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getBody(), null, null);
        } else {
            Log.d("TOKEN", "From: " + remoteMessage.getFrom());
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody  Message body
     * @param type         Message type
     * @param jsonUserData Friend data
     */
    private void sendNotification(String messageBody, String type, String jsonUserData)
    {
        PendingIntent pendingIntent = getPendingIntent(type, jsonUserData);
        NotificationCompat.Builder notificationBuilder = getNotificationBuilder(
                pendingIntent,
                getString(R.string.notification_friendship_title),
                messageBody
        );

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    getString(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }

    /**
     * Get a notification builder
     *
     * @param pendingIntent Pending intent
     * @param title         Title of the notification
     * @param message       Notification message
     * @return The notification builder
     */
    private NotificationCompat.Builder getNotificationBuilder(PendingIntent pendingIntent,
                                                              String title, String message)
    {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        //TODO change the person icon with Chefs app logo
        return new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_person_primary_48dp)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
    }

    /**
     * Get instance of pending intent for given class
     *
     * @param type         Activity class
     * @param jsonUserData User data
     * @return Pending intent
     */
    private PendingIntent getPendingIntent(String type, String jsonUserData)
    {
        Intent intent = new Intent(this, getActivityClassByType(type));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("friend", jsonUserData);

        return PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
    }

    private Class<?> getActivityClassByType(String type)
    {
        switch (type) {
            case NOTIFICATION_TYPE_REQUEST_ACCEPTED:
                return FriendsActivity.class;
            case NOTIFICATION_TYPE_NEW_USER:
            default:
                return ManageFriendshipRequestActivity.class;
        }

    }
}
