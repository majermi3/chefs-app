package com.majernik.chefsapp.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.google.gson.Gson;
import com.majernik.chefsapp.activity.LoginActivity;
import com.majernik.chefsapp.activity.MainActivity;
import com.majernik.chefsapp.api.ApiInterface;
import com.majernik.chefsapp.api.SecureApiClient;
import com.majernik.chefsapp.model.User;
import com.majernik.chefsapp.utility.StringUtility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

abstract class BaseService
{
    protected SharedPreferences sharedPref;
    private Context context;

    public BaseService(SharedPreferences sharedPref)
    {
        this.sharedPref = sharedPref;
    }

    public BaseService(Context context)
    {
        this.context = context;
        this.sharedPref = context.getSharedPreferences("USER", Context.MODE_PRIVATE);
    }

    public Context getContext()
    {
        return context;
    }

    public void setContext(Context context)
    {
        this.context = context;
    }

    public SharedPreferences getSharedPref()
    {
        return sharedPref;
    }

    public String getRefreshToken()
    {
        String refreshToken = sharedPref.getString("refreshToken", null);
        if (refreshToken != null) {
            return StringUtility.rotateByThird(refreshToken,
                    StringUtility.ROTATION_DIRECTION_RIGHT);
        }
        return null;
    }

    public void saveRefreshToken(String token)
    {
        SharedPreferences.Editor editor = sharedPref.edit();
        if (token != null) {
            token = StringUtility.rotateByThird(token, StringUtility.ROTATION_DIRECTION_LEFT);
        }
        editor.putString("refreshToken", token);
        editor.apply();
    }

    public void clearRefreshToken()
    {
        saveRefreshToken(null);
    }

    protected User getUser()
    {
        Gson gson = new Gson();
        String userJson = sharedPref.getString("user", null);

        if (userJson != null) {
            return gson.fromJson(userJson, User.class);
        }
        return null;
    }

    public void saveUser(User user)
    {
        Gson gson = new Gson();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("user", gson.toJson(user));
        editor.apply();
    }

    public void removeUser()
    {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("user");
        editor.apply();
    }

    public void handleErrorResponse(Response response)
    {
        if (response.code() == 401 || response.code() == 403) {
            deleteRefreshToken();
            redirectToHome();
        }
    }

    protected void redirectToLoginPage()
    {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    protected void redirectToHome()
    {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    protected void deleteRefreshToken()
    {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("refreshToken");
        editor.commit();
    }

    protected ApiInterface getSecureClient()
    {
        return SecureApiClient
                .getClient(context)
                .create(ApiInterface.class);
    }

    protected MultipartBody.Part getImageMultipartBody(Bitmap image) throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] data = bos.toByteArray();

        return getMultipartBodyFile(data, getRandomPngName());
    }

    protected MultipartBody.Part getVideoMultipartBody(File file)
    {
        RequestBody videoBody = RequestBody.create(MediaType.parse("video/mp4"), file);

        return MultipartBody.Part.createFormData("video", getRandomMp4Name(), videoBody);
    }

    private MultipartBody.Part getMultipartBodyFile(byte[] data, String fileName) throws IOException
    {
        File f = getFile(data, fileName);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), f);

        return MultipartBody.Part.createFormData("upload", fileName, reqFile);
    }

    private File getFile(byte[] data, String filename) throws IOException
    {
        File f = new File(context.getCacheDir(), filename);
        f.createNewFile();
        writeBytesToFile(f, data);

        return f;
    }

    private void writeBytesToFile(File f, byte[] data) throws IOException
    {
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(data);
        fos.flush();
        fos.close();
    }

    private String getRandomMp4Name()
    {
        return getRandomName() + ".mp4";
    }

    private String getRandomPngName()
    {
        return getRandomName() + ".png";
    }

    private String getRandomName()
    {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
