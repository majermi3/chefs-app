package com.majernik.chefsapp.service;

import android.content.Context;
import android.graphics.Bitmap;

import com.majernik.chefsapp.api.ApiInterface;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.api.response.RecipesResponse;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.utility.Session;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeService extends BaseService implements ApiServiceInterface.RecipeService
{
    public RecipeService(Context context)
    {
        super(context);
    }

    @Override
    public void getRecipes(
            final ApiServiceInterface.RecipeService.OnFinishedListener onFinishedListener)
    {

        ApiInterface apiService = getSecureClient();
        Call<RecipesResponse> call = apiService.getRecipes(Session.getToken());

        enqueueList(call, onFinishedListener);
    }


    @Override
    public void getMyRecipes(
            final ApiServiceInterface.RecipeService.OnFinishedListener onFinishedListener)
    {

        ApiInterface apiService = getSecureClient();
        Call<RecipesResponse> call = apiService.getMyRecipes(Session.getToken());

        enqueueList(call, onFinishedListener);
    }

    public void saveRecipe(Recipe recipe, ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        if (recipe.getId() != null) {
            updateRecipe(recipe, onFinishedListener);
        } else {
            createRecipe(recipe, onFinishedListener);
        }
    }

    public void createRecipe(Recipe recipe,
                             final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.createRecipe(Session.getToken(), recipe);

        enqueue(call, onFinishedListener);
    }

    public void updateRecipe(Recipe recipe,
                             ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.updateRecipe(Session.getToken(), recipe);

        enqueue(call, onFinishedListener);
    }

    public void deleteRecipe(Recipe recipe,
                             ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.deleteRecipe(Session.getToken(), recipe.getId());

        enqueue(call, onFinishedListener);
    }

    public void uploadRecipeImage(Recipe recipe, Bitmap image,
                                  final ApiServiceInterface.OnMediaUploadedListener onFinishedListener) throws IOException
    {
        ApiInterface apiService = getSecureClient();
        Call<MediaResponse> call = apiService.uploadRecipeImage(Session.getToken(), recipe.getId(),
                getImageMultipartBody(image));

        call.enqueue(new Callback<MediaResponse>()
        {
            @Override
            public void onResponse(Call<MediaResponse> call, Response<MediaResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<MediaResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    private void enqueueList(Call<RecipesResponse> call,
                             final ApiServiceInterface.RecipeService.OnFinishedListener onFinishedListener)
    {
        call.enqueue(new Callback<RecipesResponse>()
        {
            @Override
            public void onResponse(Call<RecipesResponse> call, Response<RecipesResponse> response)
            {
                if (response.isSuccessful()) {
                    onFinishedListener.onFinished(response.body());
                } else {
                    handleErrorResponse(response);
                }
            }

            @Override
            public void onFailure(Call<RecipesResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    private void enqueue(Call<ApiResponse> call,
                         final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        call.enqueue(new Callback<ApiResponse>()
        {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
