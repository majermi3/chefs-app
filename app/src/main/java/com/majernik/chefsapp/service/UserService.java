package com.majernik.chefsapp.service;

import android.content.Context;
import android.graphics.Bitmap;

import com.majernik.chefsapp.api.ApiClient;
import com.majernik.chefsapp.api.ApiInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.MediaResponse;
import com.majernik.chefsapp.api.response.TokenResponse;
import com.majernik.chefsapp.api.response.UserResponse;
import com.majernik.chefsapp.api.response.UsersResponse;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.User;
import com.majernik.chefsapp.utility.Session;

import java.io.IOException;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserService extends BaseService implements ApiServiceInterface.UserService
{
    public UserService(Context context)
    {
        super(context);
    }

    public void getCurrentUser(
            final ApiServiceInterface.UserService.OnFinishedListener onFinishedListener)
    {
        User currentUser = getUser();
        if (currentUser != null) {
            onFinishedListener.onFinished(new UserResponse(currentUser));
        } else if (Session.hasToken()) {
            ApiInterface apiService = getSecureClient();
            Call<UserResponse> call = apiService.getCurrentUser(Session.getToken());

            call.enqueue(new Callback<UserResponse>()
            {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response)
                {
                    if (response.isSuccessful()) {
                        UserResponse userResponse = response.body();
                        saveUser(userResponse.getUser());
                        onFinishedListener.onFinished(userResponse);
                    } else {
                        handleErrorResponse(response);
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t)
                {
                    onFinishedListener.onFailure(t);
                }
            });
        }
    }

    public void findUsers(String term, final OnUsersFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<UsersResponse> call = apiService.findUsers(Session.getToken(), term);

        call.enqueue(new Callback<UsersResponse>()
        {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response)
            {
                if (response.isSuccessful()) {
                    UsersResponse usersResponse = response.body();
                    onFinishedListener.onFinished(usersResponse);
                } else {
                    handleErrorResponse(response);
                }
            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    public void rejectFriendshipRequest(String friendId,
                                        ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.rejectFriendshipRequest(Session.getToken(), friendId);

        enqueue(call, onFinishedListener);
    }

    public void acceptFriendshipRequest(String friendId,
                                        ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.acceptFriendshipRequest(Session.getToken(), friendId);

        enqueue(call, onFinishedListener);
    }

    public void sendFriendshipRequest(String friendId,
                                      final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.sendFriendshipRequest(Session.getToken(), friendId);

        enqueue(call, onFinishedListener);
    }

    public void removeFriendship(String friendId,
                                 final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.removeFriendship(Session.getToken(), friendId);

        enqueue(call, onFinishedListener);
    }

    @Override
    public void createUser(User user,
                           final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ApiResponse> call = apiService.createUser(user);

        enqueue(call, onFinishedListener);
    }

    public void updateCurrentUser(User user)
    {
        updateCurrentUser(user, null);
    }

    @Override
    public void updateCurrentUser(User user,
                                  final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.updateCurrentUser(Session.getToken(), user);

        enqueue(call, onFinishedListener);
    }

    public void uploadRecipeStepImage(Bitmap image,
                                      final ApiServiceInterface.OnMediaUploadedListener onFinishedListener) throws IOException
    {
        MultipartBody.Part body = getImageMultipartBody(image);
        ApiInterface apiService = getSecureClient();

        Call<MediaResponse> call = apiService.uploadProfileImage(Session.getToken(), body);

        enqueue(call, onFinishedListener);
    }

    @Override
    public void markRecipeAsFavorite(Recipe recipe,
                                     ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.markRecipeAsFavorite(Session.getToken(),
                recipe.getId());

        enqueue(call, onFinishedListener);
    }

    @Override
    public void unmarkRecipeAsFavorite(Recipe recipe,
                                       ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.unmarkRecipeAsFavorite(Session.getToken(),
                recipe.getId());

        enqueue(call, onFinishedListener);
    }

    public void loginUser(String email, final String token, String provider,
                          final ApiServiceInterface.TokenService.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TokenResponse> call = apiService.loginUser(email, token, provider);

        call.enqueue(new Callback<TokenResponse>()
        {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response)
            {
                TokenResponse tokenResponse = response.body();

                Session.setToken(token);

                onFinishedListener.onFinished(tokenResponse);
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    @Override
    public void loginUser(String email, String password,
                          final ApiServiceInterface.TokenService.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TokenResponse> call = apiService.loginUser(email, password);

        call.enqueue(new Callback<TokenResponse>()
        {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response)
            {
                TokenResponse tokenResponse = response.body();

                Session.setToken(tokenResponse.getToken());
                saveRefreshToken(tokenResponse.getRefreshToken());

                onFinishedListener.onFinished(tokenResponse);
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    public void logout()
    {
        removeUser();
        Session.clearToken();
        clearRefreshToken();
    }

    private void enqueue(Call<ApiResponse> call,
                         final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        call.enqueue(new Callback<ApiResponse>()
        {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response)
            {
                if (onFinishedListener != null) {
                    onFinishedListener.onFinished(response.body());
                }
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t)
            {
                if (onFinishedListener != null) {
                    onFinishedListener.onFailure(t);
                }
            }
        });
    }

    private void enqueue(Call<MediaResponse> call,
                         final ApiServiceInterface.OnMediaUploadedListener onFinishedListener)
    {
        call.enqueue(new Callback<MediaResponse>()
        {
            @Override
            public void onResponse(Call<MediaResponse> call, Response<MediaResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<MediaResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
