package com.majernik.chefsapp.service;

import android.content.Context;

import com.majernik.chefsapp.api.ApiInterface;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.ApiResponse;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.utility.Session;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeIngredientsService extends BaseService implements ApiServiceInterface.RecipeIngredientService
{
    public RecipeIngredientsService(Context context)
    {
        super(context);
    }

    public void saveRecipeIngredient(Recipe recipe, RecipeIngredient recipeIngredient,
                                     ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        if (recipeIngredient.getId() != null) {
            updateRecipeIngredient(recipe, recipeIngredient, onFinishedListener);
        } else {
            createRecipeIngredient(recipe, recipeIngredient, onFinishedListener);
        }
    }

    public void updateRecipeIngredient(final Recipe recipe, final RecipeIngredient recipeIngredient,
                                       final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.updateRecipeIngredient(Session.getToken(),
                recipe.getId(), recipeIngredient.getId(), recipeIngredient);

        call.enqueue(new Callback<ApiResponse>()
        {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    public void createRecipeIngredient(final Recipe recipe,
                                       final RecipeIngredient recipeIngredient,
                                       final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.createRecipeIngredient(Session.getToken(),
                recipe.getId(), recipeIngredient);

        call.enqueue(new Callback<ApiResponse>()
        {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    public void removeRecipeIngredient(final Recipe recipe,
                                       final RecipeIngredient recipeIngredient,
                                       final ApiServiceInterface.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = getSecureClient();
        Call<ApiResponse> call = apiService.removeRecipeIngredient(Session.getToken(),
                recipe.getId(), recipeIngredient.getId());

        call.enqueue(new Callback<ApiResponse>()
        {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response)
            {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
