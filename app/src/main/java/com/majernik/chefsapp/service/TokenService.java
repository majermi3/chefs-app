package com.majernik.chefsapp.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.majernik.chefsapp.R;
import com.majernik.chefsapp.api.ApiClient;
import com.majernik.chefsapp.api.ApiInterface;
import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.api.response.TokenResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

public class TokenService extends BaseService implements ApiServiceInterface.TokenService
{
    public TokenService(SharedPreferences sharedPref)
    {
        super(sharedPref);
    }

    public TokenService(Context context)
    {
        super(context);
    }

    public GoogleSignInAccount oauthSilentSignIn()
    {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(
                GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getContext().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(getContext(), gso);
        Task<GoogleSignInAccount> task = googleSignInClient.silentSignIn();
        try {
            if (task.isSuccessful()) {
                return task.getResult(ApiException.class);
            } else {
                return null;
            }
        } catch (ApiException e) {
            return null;
        }
    }

    public void oauth(String token,
                      final ApiServiceInterface.TokenService.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TokenResponse> call = apiService.oauth(token);

        call.enqueue(new Callback<TokenResponse>()
        {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response)
            {
                if (response.isSuccessful()) {
                    onFinishedListener.onFinished(response.body());
                } else {
                    handleErrorResponse(response);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

    public Response<TokenResponse> requestToken()
    {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        try {
            Call<TokenResponse> call = apiService.getToken(getRefreshToken());
            return call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void requestAsyncToken(
            final ApiServiceInterface.TokenService.OnFinishedListener onFinishedListener)
    {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TokenResponse> call = apiService.getToken(getRefreshToken());

        call.enqueue(new Callback<TokenResponse>()
        {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response)
            {
                if (response.isSuccessful()) {
                    onFinishedListener.onFinished(response.body());
                } else {
                    handleErrorResponse(response);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t)
            {
                onFinishedListener.onFailure(t);
            }
        });
    }

}
