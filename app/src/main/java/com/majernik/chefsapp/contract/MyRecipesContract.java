package com.majernik.chefsapp.contract;

public interface MyRecipesContract
{
    interface View
    {

    }

    interface Presenter
    {
        void loadMyRecipes();
    }
}
