package com.majernik.chefsapp.contract;

import android.content.Context;

import com.majernik.chefsapp.model.User;

public interface SignUpContract
{
    interface View
    {
        Context getApplicationContext();

        void showLogin();

        void showError(String message);
    }

    interface Presenter
    {
        void signUp(User user);
    }
}
