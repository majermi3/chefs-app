package com.majernik.chefsapp.contract;

import android.content.Context;
import android.graphics.Bitmap;

import com.majernik.chefsapp.activity.popup.IngredientsPopup;
import com.majernik.chefsapp.model.Ingredient;
import com.majernik.chefsapp.model.Recipe;
import com.majernik.chefsapp.model.RecipeIngredient;
import com.majernik.chefsapp.model.RecipeStep;

import java.util.List;
import java.util.TreeMap;

public interface CreateRecipeContract
{
    interface View
    {
        Context getApplicationContext();

        IngredientsPopup getIngredientsPopup();

        void addIngredientToView(RecipeIngredient ingredient);

        void removeIngredientFromView(RecipeIngredient ingredient);

        void setResultIntent(Recipe recipe);

        void notifyStepAdded();

        void notifyStepChanged(RecipeStep step);

        void notifyStepsChanged(List<RecipeStep> steps);
    }

    interface Popup
    {
        void showIngredients(TreeMap<Integer, Ingredient> ingredients);
    }

    interface Presenter
    {
        void loadIngredients();

        void createIngredient(RecipeIngredient recipeIngredient);

        void removeIngredient(RecipeIngredient ingredient);

        void findIngredients(CharSequence s);

        void persistRecipe();

        void setRecipeTitle(String title);

        void setRecipeDescription(String description);

        void setRecipeAccessLevel(int accessLevel);

        void addRecipeStep(RecipeStep step);

        void uploadRecipeImage(Bitmap image);
    }
}
