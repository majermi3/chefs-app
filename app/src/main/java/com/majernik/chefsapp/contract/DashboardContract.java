package com.majernik.chefsapp.contract;

import android.content.Context;

import com.majernik.chefsapp.api.ApiServiceInterface;
import com.majernik.chefsapp.model.Recipe;

import java.util.List;

public interface DashboardContract
{
    interface OnFragmentInteractionListener
    {
    }

    interface View
    {
        Context getApplicationContext();

        void addRecipes(List<Recipe> recipes);

        void showError(String message);

        void redirectToLogin();
    }

    interface Presenter
    {
        void markRecipeAsFavorite(Recipe recipe,
                                  ApiServiceInterface.OnFinishedListener onFinishedListener);

        void unmarkRecipeAsFavorite(Recipe recipe,
                                    ApiServiceInterface.OnFinishedListener onFinishedListener);
    }
}
