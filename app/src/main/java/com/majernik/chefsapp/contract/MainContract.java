package com.majernik.chefsapp.contract;

public interface MainContract
{
    interface View
    {
        void showLogin();

        void showSignUp();
    }

    interface Presenter
    {
        void login();

        void register();
    }
}
