package com.majernik.chefsapp.contract;

import android.content.Context;

public interface LoginContract
{
    interface View
    {
        Context getApplicationContext();

        void showDashboard();

        void showError(String message);
    }

    interface Presenter
    {
        void loginUser(String email, String password);
    }
}
