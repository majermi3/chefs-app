package com.majernik.chefsapp.contract;

import android.content.Context;

import com.majernik.chefsapp.model.Recipe;

public interface DetailContract
{
    interface Presenter
    {
        void deleteRecipe(Recipe recipe);
    }

    interface View
    {
        Context getApplicationContext();

        void showMyRecipesView();

    }
}
