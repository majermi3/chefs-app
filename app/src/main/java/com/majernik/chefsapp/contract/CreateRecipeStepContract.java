package com.majernik.chefsapp.contract;

import android.content.Context;
import android.graphics.Bitmap;

public interface CreateRecipeStepContract
{
    interface View
    {
        Context getApplicationContext();
    }

    interface Presenter
    {
        void uploadRecipeStepImage(Bitmap imageBitmap);
    }
}
