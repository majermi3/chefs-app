package com.majernik.chefsapp.model;

import com.majernik.chefsapp.utility.FileExtensionUtility;

import java.util.ArrayList;

public class User extends AbstractModel
{
    protected String name;
    protected String email;
    protected String image;
    protected String password;
    protected String fcmRefreshToken;
    protected String provider;
    protected String providerId;
    protected Recipe[] favoriteRecipes;
    protected ArrayList<User> friends;
    protected ArrayList<User> pendingRequests;
    protected boolean invalid = false;

    public final static String PROVIDER_CHEF = "chefs_app";
    public final static String PROVIDER_GOOGLE = "google";

    public User()
    {
    }

    public User(String name, String email, String password)
    {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public User(String name, String email, String password, String image)
    {
        this.name = name;
        this.email = email;
        this.password = password;
        this.image = image;
    }

    public User(String name, String email, String password, String image, String fcmRefreshToken)
    {
        this.name = name;
        this.email = email;
        this.password = password;
        this.image = image;
        this.fcmRefreshToken = fcmRefreshToken;
    }

    public User(String name, String email, String image, String fcmRefreshToken,
                Recipe[] favoriteRecipes)
    {
        this.name = name;
        this.email = email;
        this.image = image;
        this.fcmRefreshToken = fcmRefreshToken;
        this.favoriteRecipes = favoriteRecipes;
    }

    public User(String name, String email, String image, String fcmRefreshToken,
                Recipe[] favoriteRecipes, ArrayList<User> friends)
    {
        this.name = name;
        this.email = email;
        this.image = image;
        this.fcmRefreshToken = fcmRefreshToken;
        this.favoriteRecipes = favoriteRecipes;
        this.friends = friends;
    }

    public User(String name, String email, String image, String fcmRefreshToken,
                Recipe[] favoriteRecipes, ArrayList<User> friends, ArrayList<User> pendingRequests)
    {
        this.name = name;
        this.email = email;
        this.image = image;
        this.fcmRefreshToken = fcmRefreshToken;
        this.favoriteRecipes = favoriteRecipes;
        this.friends = friends;
        this.pendingRequests = pendingRequests;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getProvider()
    {
        return provider;
    }

    public void setProvider(String provider)
    {
        this.provider = provider;
    }

    public String getProviderId()
    {
        return providerId;
    }

    public void setProviderId(String providerId)
    {
        this.providerId = providerId;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public boolean hasImage()
    {
        return this.getImage() != null
                && !this.getImage().isEmpty()
                && FileExtensionUtility.isImage(this.getImage());
    }

    public boolean hasFcmRefreshToken()
    {
        return this.getFcmRefreshToken() != null && !this.getFcmRefreshToken().isEmpty();
    }

    public String getFcmRefreshToken()
    {
        return fcmRefreshToken;
    }

    public void setFcmRefreshToken(String fcmRefreshToken)
    {
        this.fcmRefreshToken = fcmRefreshToken;
    }

    public Recipe[] getFavoriteRecipes()
    {
        return favoriteRecipes;
    }

    public boolean hasFavorteRecipe(Recipe recipe)
    {
        for (Recipe favoriteRecipe : favoriteRecipes) {
            if (favoriteRecipe.getId().equals(recipe.getId())) {
                return true;
            }
        }
        return false;
    }

    public void setFavoriteRecipes(Recipe[] favoriteRecipes)
    {
        this.favoriteRecipes = favoriteRecipes;
    }

    public boolean isFriend(User user)
    {
        for (User friend : friends) {
            if (friend.getId() == user.getId()) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<User> getFriends()
    {
        return friends;
    }

    public void addFriend(User friend)
    {
        friends.add(friend);
    }

    public void removeFriend(User friend)
    {
        friends.remove(friend);
    }

    public void setFriends(ArrayList<User> friends)
    {
        this.friends = friends;
    }

    public ArrayList<User> getPendingRequests()
    {
        return pendingRequests;
    }

    public void addPendingRequest(User user)
    {
        pendingRequests.add(user);
    }

    public void removePendingRequest(User user)
    {
        pendingRequests.remove(user);
    }

    public void setPendingRequests(ArrayList<User> pendingRequests)
    {
        this.pendingRequests = pendingRequests;
    }

    public boolean isInvalid()
    {
        return invalid;
    }

    public void setInvalid(boolean invalid)
    {
        this.invalid = invalid;
    }

    public void invalidate()
    {
        this.setInvalid(true);
    }
}
