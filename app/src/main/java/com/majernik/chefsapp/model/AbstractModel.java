package com.majernik.chefsapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

abstract public class AbstractModel implements Serializable
{
    @SerializedName("_id")
    protected String id;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
}
