package com.majernik.chefsapp.model;

import android.content.res.Resources;

import com.majernik.chefsapp.utility.StringUtility;
import com.majernik.chefsapp.utility.UnitsUtility;

public class RecipeIngredient extends AbstractModel
{
    private Ingredient ingredient;
    private Integer unit;
    private Double amount;
    private boolean metricUnitSystem = true;

    public RecipeIngredient()
    {

    }

    public RecipeIngredient(Ingredient ingredient)
    {
        this.ingredient = ingredient;
    }

    public RecipeIngredient(Ingredient ingredient, Integer unit, Double amount)
    {
        this.ingredient = ingredient;
        this.unit = unit;
        this.amount = amount;
    }

    public RecipeIngredient(Ingredient ingredient, Integer unit, Double amount,
                            boolean metricUnitSystem)
    {
        this.ingredient = ingredient;
        this.unit = unit;
        this.amount = amount;
        this.metricUnitSystem = metricUnitSystem;
    }

    public Ingredient getIngredient()
    {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient)
    {
        this.ingredient = ingredient;
    }

    public Integer getUnit()
    {
        return unit;
    }

    public void setUnit(Integer unit)
    {
        this.unit = unit;
    }

    public Double getAmount()
    {
        return amount;
    }

    public String getFormattedAmount(Settings settings)
    {
        return StringUtility.getFormattedDoubleWithFractions(
                UnitsUtility.getConvertedAmount(settings, this)
        );
    }

    public void setAmount(Double amount)
    {
        this.amount = amount;
    }

    public void addAmount(Double amount)
    {
        this.amount += amount;
    }

    public boolean isMetricUnitSystem()
    {
        return metricUnitSystem;
    }

    public void setMetricUnitSystem(boolean metricUnitSystem)
    {
        this.metricUnitSystem = metricUnitSystem;
    }

    public String toString(Resources resources, Settings settings)
    {
        if (getUnit() != null) {
            return getFormattedAmount(settings)
                    + " " + getUnitName(resources, settings)
                    + " " + getIngredient().getTitle();
        } else {
            return getIngredient().getTitle();
        }
    }

    private String getUnitName(Resources resources, Settings settings)
    {
        return UnitsUtility.getUnitName(resources, settings, getUnit(), getIngredient().isLiquid());
    }
}
