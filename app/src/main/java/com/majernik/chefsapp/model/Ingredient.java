package com.majernik.chefsapp.model;

public class Ingredient extends AbstractModel
{

    private String title;

    private IngredientType type;

    private boolean liquid;

    public Ingredient()
    {
    }

    public Ingredient(String title)
    {
        this.title = title;
    }

    public Ingredient(String title, IngredientType type, boolean liquid)
    {
        this.title = title;
        this.type = type;
        this.liquid = liquid;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public IngredientType getType()
    {
        return type;
    }

    public void setType(IngredientType type)
    {
        this.type = type;
    }

    public boolean isLiquid()
    {
        return liquid;
    }

    public void setLiquid(boolean liquid)
    {
        this.liquid = liquid;
    }
}
