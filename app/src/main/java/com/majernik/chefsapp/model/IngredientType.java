package com.majernik.chefsapp.model;

public class IngredientType extends AbstractModel
{
    private String title;

    public IngredientType()
    {
    }

    public IngredientType(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}
