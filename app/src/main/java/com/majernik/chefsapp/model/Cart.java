package com.majernik.chefsapp.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cart
{
    private Map<String, RecipeIngredient> recipeIngredients;

    public Cart()
    {
        recipeIngredients = new HashMap<>();
    }

    public void addRecipe(Recipe recipe)
    {
        addRecipeIngredients(recipe.getIngredients());
    }

    public void removeRecipeIngredient(RecipeIngredient recipeIngredient)
    {
        this.recipeIngredients.remove(recipeIngredient.getId());
    }

    public void addRecipeIngredients(List<RecipeIngredient> recipeIngredients)
    {
        for (RecipeIngredient ri : recipeIngredients) {
            if (!this.recipeIngredients.containsKey(ri.getId())) {
                this.recipeIngredients.put(ri.getId(), ri);
            } else {
                this.recipeIngredients.get(ri.getId()).addAmount(ri.getAmount());
            }
        }
    }

    public Map<String, RecipeIngredient> getRecipeIngredients()
    {
        return recipeIngredients;
    }
}
