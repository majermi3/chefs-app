package com.majernik.chefsapp.model;

public class Settings
{
    public static final int UNITS_METRIC = 0;
    public static final int UNITS_IMPERIAL = 1;

    private int unitsSystem;

    public Settings()
    {
    }

    public Settings(int unitsSystem)
    {
        this.unitsSystem = unitsSystem;
    }

    public int getUnitsSystem()
    {
        return unitsSystem;
    }

    public boolean isMetricSystem()
    {
        return getUnitsSystem() == UNITS_METRIC;
    }

    public void setUnitsSystem(int unitsSystem)
    {
        this.unitsSystem = unitsSystem;
    }
}
