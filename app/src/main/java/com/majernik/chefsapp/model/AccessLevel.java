package com.majernik.chefsapp.model;

import com.majernik.chefsapp.R;

public enum AccessLevel
{
    PRIVATE(1, R.string.access_level_private),
    FRIEND(2, R.string.access_level_friend),
    FOF(3, R.string.access_level_fof),
    PUBLIC(4, R.string.access_level_public);

    private final int value;
    private final int titleId;

    AccessLevel(int value, int titleId)
    {
        this.value = value;
        this.titleId = titleId;
    }

    public int getValue()
    {
        return value;
    }

    public int getTitleId()
    {
        return titleId;
    }
}
