package com.majernik.chefsapp.model;

import com.majernik.chefsapp.utility.FileExtensionUtility;

public class RecipeStep extends AbstractModel
{
    /**
     * Description
     */
    private String description;

    /**
     * Position of the step
     */
    private int position;

    /**
     * URL of the media file (image/video)
     */
    private String media;

    /**
     * URL of the thumbnail
     */
    private String thumbnail;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getPosition()
    {
        return position;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }

    public boolean isEmpty()
    {
        return description.isEmpty();
    }

    public String getThumbnail()
    {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail)
    {
        this.thumbnail = thumbnail;
    }

    public boolean hasThumbnail()
    {
        return thumbnail != null && !thumbnail.isEmpty();
    }

    public String getMedia()
    {
        return media;
    }

    public void setMedia(String media)
    {
        this.media = media;
    }

    public boolean hasImage()
    {
        return hasMedia() && FileExtensionUtility.isImage(this.media);
    }

    public boolean hasMedia()
    {
        return media != null && !media.isEmpty();
    }

    public boolean hasVideo()
    {
        return hasMedia() && FileExtensionUtility.isVideo(this.media);
    }
}
