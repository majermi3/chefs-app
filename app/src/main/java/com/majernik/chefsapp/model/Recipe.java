package com.majernik.chefsapp.model;

import java.util.ArrayList;
import java.util.List;

public class Recipe extends AbstractModel
{
    private String title;
    private String description;
    private String image;
    private int accessLevel;
    private List<RecipeIngredient> ingredients;
    private List<RecipeStep> steps;

    public Recipe()
    {
        ingredients = new ArrayList<>();
        steps = new ArrayList<>();
    }

    public Recipe(String title, List<RecipeIngredient> ingredients, List<RecipeStep> steps)
    {
        this.title = title;
        this.ingredients = ingredients;
        this.steps = steps;
    }

    public Recipe(String title, String description, int accessLevel,
                  List<RecipeIngredient> ingredients, List<RecipeStep> steps)
    {
        this.title = title;
        this.ingredients = ingredients;
        this.description = description;
        this.accessLevel = accessLevel;
        this.steps = steps;
    }

    public Recipe(String title, String description, String image, int accessLevel,
                  List<RecipeIngredient> ingredients, List<RecipeStep> steps)
    {
        this.title = title;
        this.description = description;
        this.image = image;
        this.accessLevel = accessLevel;
        this.ingredients = ingredients;
        this.steps = steps;
    }

    public boolean hasTitle()
    {
        return title != null && !title.isEmpty();
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public boolean hasImage()
    {
        return image != null && !image.isEmpty();
    }

    public int getAccessLevel()
    {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel)
    {
        this.accessLevel = accessLevel;
    }

    public List<RecipeIngredient> getIngredients()
    {
        return ingredients;
    }

    public void setIngredients(List<RecipeIngredient> ingredients)
    {
        this.ingredients = ingredients;
    }

    public void addIngredient(RecipeIngredient ingredient)
    {
        ingredients.add(ingredient);
    }

    public void removeIngredient(RecipeIngredient ingredient)
    {
        ingredients.remove(ingredient);
    }

    public List<RecipeStep> getSteps()
    {
        return steps;
    }

    public void setSteps(List<RecipeStep> steps)
    {
        this.steps = steps;
    }

    public void setStep(RecipeStep step)
    {
        for (int i = 0; i < steps.size(); i++) {
            RecipeStep s = steps.get(i);
            if (step.getId().equals(s.getId())) {
                steps.set(i, step);
                break;
            }
        }
    }

    public void addStep(RecipeStep step)
    {
        steps.add(step);
    }

    public void removeStep(RecipeStep step)
    {
        steps.remove(step);
    }

    public boolean isEmpty()
    {
        return this.ingredients.size() == 0 && !this.hasTitle();
    }
}
